<table class='table text-secondary table-hover text-center table-responsive-lg'>
    <tr class="">
        <td colspan=4 class='align-middle'>
            <form method="GET" id="filtro" class="form-inline mx-2 my-2 my-lg-0">
                <input type="hidden" name="page" value="listaConFis">
                <div class="input-group-prepend">
                    <span class="input-group-text">Por Nome</span>
                    <input type="search" class="form-control mr-sm-2 termo1 mb-auto" name="b[nome]" placeholder="Nome do Funcionário">
                </div>
                <button class="btn btn-outline-success mx-2 my-2 my-sm-0 buscar" type="submit" disabled>Buscar</button>
                <a href="<?= $url_site ?>listaConFis" class="btn btn-outline-danger">LIMPAR BUSCA</a>
            </form>
        </td>
        <td colspan=2 class='align-middle'><a href="<?= $url_site ?>cadastroConFis" class="text-light btn btn-primary amarelo" style='width: 100%;'>Adicionar <i class="icofont-ui-add"></i></a></td>
    </tr>
    <tr class='thead-dark'>
        <th class='align-middle'>Nome do Condominio</th>
        <th class='align-middle'>Função</th>
        <th class='align-middle'>Nome</th>
        <th class='align-middle'>Criação:</th>
        <th class='align-middle'>Atualização:</th>
        <th class='align-middle'></th>
    </tr>
    <? foreach ($result['resultSet'] as $ch => $confis) { ?>
        <tr class='text-center flex-wrap' data-id='<?= $confis['id'] ?>'>
            <td class='align-middle'><?= $confis['nomeCondo'] ?></td>
            <td class='align-middle'><?= $confis['funcao'] ?></td>
            <td class='align-middle'><?= $confis['nome'] ?></td>
            <td class='align-middle'><?= dateFormat($confis['dataCadastro']) ?></td>
            <td class='align-middle'><?= dateFormat($confis['dataUpdate']) ?></td>
            <td class='align-middle'>
                <a href='<?= $url_site ?>cadastroConFis/id/<?= $confis['id'] ?>' class="texto-amarelo"><i class="icofont-edit-alt"></i></a>
                <a href='#' data-id="<?= $confis['id'] ?>" class='removerConFis texto-amarelo'><i class="icofont-ui-delete"></i></a>
            </td>
        </tr>
    <? } ?>
</table>
<div class="col-sm-12">
    <div class="row">
        <? if ($paginacao) { ?>
            <div class="col-12 col-sm-12 col-md-7 col-lg-5">
                <div><?= $paginacao ?></div>
            </div>
        <? } ?>
        <div class="col-4">
            <span class="text-light">Total de Registros:</span> <span class="badge badge-secondary total"> <?= $totalRegistros ?></span>
        </div>
    </div>
</div>