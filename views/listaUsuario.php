<table class='table text-secondary table-hover text-center table-responsive-lg'>
        <tr class="">
            <td colspan=5 class='align-middle'>
            <form method="GET" id="filtro" class="form-inline mx-2 my-2 my-lg-0">
                <input type="hidden" name="page" value="listaUsuario">
                <div class="input-group-prepend">
                    <span class="input-group-text">Por Nome</span>
                    <input type="search" class="form-control mr-sm-2 termo1 mb-auto" name="b[nome]" placeholder="Usuarios">
                </div>
                <button class="btn btn-outline-success mx-2 my-2 my-sm-0 buscar" type="submit" disabled>Buscar</button>
                <a href="<?= $url_site ?>listaUsuario" class="btn btn-outline-danger">LIMPAR BUSCA</a>
            </form>
            </td>
            <td colspan=2 class='align-middle'><a href="<?= $url_site?>cadastroUsuario" class="text-light btn btn-primary amarelo" style='width: 100%;'>Adicionar <i class="icofont-ui-add"></i></a></td>
        </tr>
        <tr class='thead-dark'>
            <th class='align-middle'>Nome do Usuario</th>
            <th class='align-middle'>Login do Usuario</th>
            <th class='align-middle'>Senha do Usuario</th>
            <th class='align-middle'>Criação:</th>
            <th class='align-middle'>Atualização:</th>
            <th class='align-middle'>Editar:</th>
            <th class='align-middle'>Deletar:</th>
        </tr>
        <?foreach($result['resultSet'] as $user) {?>
            <tr class='text-center flex-wrap' data-id='<?=$user['id']?>'>
                <td class='align-middle'><?= $user['nome'] ?></td>
                <td class='align-middle'><?= $user['usuario'] ?></td>
                <td class='align-middle'><?= $user['senha'] ?></td>
                <td class='align-middle'><?=dateFormat($user['dataCadastro'])?></td>
                <td class='align-middle'><?=dateFormat($user['dataUpdate'])?></td>
                <td class='align-middle'><a href='<?=$url_site?>cadastroUsuario/id/<?=$user['id']?>' class="texto-amarelo"><i class="icofont-edit-alt icofont-2x"></i></a></td>
                <td class='align-middle'><a href='#' data-id="<?=$user['id']?>" class='removerUsuario texto-amarelo'><i class="icofont-ui-delete icofont-2x"></i></a></td>
            </tr>
        <?}?>
</table>
<div class="col-sm-12">
<div class="row">
        <? if ($paginacao) { ?>
            <div class="col-12 col-sm-12 col-md-7 col-lg-5">
                <div><?= $paginacao ?></div>
            </div>
        <? } ?>
        <div class="col-md-4">
            <span class="text-light">Total de Registros: <span class="badge badge-secondary total"><?= $totalRegistros ?></span></span>
        </div>
    </div>
</div>
