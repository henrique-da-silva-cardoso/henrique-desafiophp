<div class="container">
    <form action="" method="post" id='formBloco' class='mt-3'>
        <div class="form-group">
            <select name="from_condominio" class="custom-select" required>
                <option value="">Condominios</option>
                <? foreach ($condominio as $condo) { ?>
                    <option value="<?=$condo['id']?>" <?= ($condo['nomeCondo'] == $popular['nomeCondo'] ? 'selected="selected"' : '') ?>><?= $condo['nomeCondo'] ?></option>
                <? } ?>
            </select>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="nomeBloco" aria-describedby="required" value='<?= $popular['nomeBloco'] ?>' placeholder="Nome do Bloco" required>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="qtAndares" aria-describedby="required" value='<?= $popular['qtAndares'] ?>' placeholder="Quantidade de Andares" required>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="qtUnidades" aria-describedby="required" value='<?= $popular['qtUnidades'] ?>' placeholder="Quantidade de Unidades por Andar" required>
        </div>
        <? if ($_GET['id']) { ?>
            <input type="hidden" name='editar' value="<?= $_GET['id'] ?>">
        <? } ?>
        <button type="submit" class="btn btn-primary amarelo texto-preto buttonEnviar">ENVIAR</button>
    </form>
</div>