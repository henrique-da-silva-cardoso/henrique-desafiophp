<table class='table text-secondary table-hover text-center table-responsive-lg'>
    <tr class="">
        <td colspan=5 class='align-middle'>
            <form method="GET" id="filtro" class="form-inline mx-2 my-2 my-lg-0">
                <input type="hidden" name="page" value="listaBloco">
                <div class="input-group-prepend">
                    <span class="input-group-text">Por Nome</span>
                    <input type="search" class="form-control mr-sm-2 termo1 mb-auto" name="b[nomeBloco]" placeholder="Nome do Bloco">
                </div>
                <button class="btn btn-outline-success mx-2 my-2 my-sm-0 buscar" type="submit" disabled>Buscar</button>
                <a href="<?= $url_site ?>listaBloco" class="btn btn-outline-danger">LIMPAR BUSCA</a>
            </form>
        </td>
        <td colspan=2 class='align-middle'><a href="<?= $url_site ?>cadastroBloco" class="text-light btn btn-primary amarelo" style='width: 100%;'>Adicionar <i class="icofont-ui-add"></i></a></td>
    </tr>

    <tr class='thead-dark'>
        <th class="align-middle">Nome do Condominio</th>
        <th class="align-middle">Nome do Bloco</th>
        <th class="align-middle">Quantidade de Andares</th>
        <th class="align-middle">Quantidade de Unidades</th>
        <th class="align-middle">Criação:</th>
        <th class="align-middle">Atualização:</th>
        <th class="align-middle"></th>
    </tr>
    <?

    foreach ($result['resultSet'] as $blocos) { ?>
        <tr class='text-center flex-wrap' data-id='<?= $blocos['id'] ?>'>
            <td class='align-middle'><?= $blocos['nomeCondo'] ?></td>
            <td class='align-middle'><?= $blocos['nomeBloco'] ?></td>
            <td class='align-middle'><?= $blocos['qtAndares'] ?></td>
            <td class='align-middle'><?= $blocos['qtUnidades'] ?></td>
            <td class='align-middle'><?= dateFormat($blocos['dataCadastro']) ?></td>
            <td class='align-middle'><?= dateFormat($blocos['dataUpdate']) ?></td>
            <td class='align-middle'>
                <a href='<?= $url_site ?>cadastroBloco/id/<?= $blocos['id']?>'><i class="icofont-edit-alt texto-amarelo"></i></a>
                <a href='#' data-id="<?= $blocos['id'] ?>" class='removerBloco texto-amarelo'><i class="icofont-ui-delete">            
            </td>
        </tr>
    <? } ?>
</table>
<div class="col-sm-12">
    <div class="row">
        <? if ($paginacao) { ?>
            <div class="col-12 col-sm-12 col-md-7 col-lg-5">
                <div><?= $paginacao ?></div>
            </div>
        <? } ?>
        <div class="col-4">
            <span class="text-light">Total de Registros:</span> <span class="badge badge-secondary total"> <?= $totalRegistros ?></span>
        </div>
    </div>
</div>