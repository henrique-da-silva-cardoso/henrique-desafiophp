<table class='table text-secondary table-hover text-center table-responsive-xl'>
    <tr class="">
        <td colspan=3 class='align-middle'>
            <form method="GET" id="filtro" class="form-inline my-2 my-lg-0">
                <div class="col-12 col-md-3">
                    <input type="hidden" name="page" value="listaAdmin">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Por CNPJ</span>
                        <!-- <input type="search" class="form-control mr-sm-2 termo1 mb-auto" name="b[cnpj]" placeholder="Nome da Admin"> -->
                        <select name="b[cnpj]" class="custom-select termo1" id="">
                                    <option value="">...</option>
                                    <? foreach ($result1 as $admin) { ?>
                                        <option value="<?= $admin['cnpj'] ?>"><?= $admin['cnpj'] ?></option>;
                                    <? } ?>
                        </select>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <button class="btn btn-outline-success mx-3 my-2 my-sm-0 buscar" type="submit" disabled>Buscar</button>
                    <a href="<?= $url_site ?>listaAdmin" class="btn btn-outline-danger">LIMPAR BUSCA</a>
                </div>
            </form>
        </td>
        <td colspan=2 class='align-middle'><a href="<?= $url_site ?>cadastroAdmin" class="text-light btn btn-primary texto-preto amarelo" style='width: 100%;'>Adicionar <i class="icofont-ui-add"></i></a></td>
    </tr>
    <tr class='thead-dark'>
        <th class='align-middle'>Nome da Administradora</th>
        <th class='align-middle'>CNPJ</th>
        <th class='align-middle'>Criação:</th>
        <th class='align-middle'>Atualização:</th>
        <th class='align-middle'></th>
    </tr>
    <? foreach ($result['resultSet'] as $admin) { ?>
        <tr class='text-center flex-wrap' data-id='<?= $admin['id'] ?>'>
            <td class='align-middle'><?= $admin['nomeAdmin'] ?></td>
            <td class='align-middle'><?= $admin['cnpj'] ?></td>
            <td class='align-middle'><?= dateFormat($admin['dataCadastro']) ?></td>
            <td class='align-middle'><?= dateFormat($admin['dataUpdate']) ?></td>
            <td class='align-middle'>
                <a href='<?= $url_site ?>cadastroAdmin/id/<?= $admin['id'] ?>'><i class="icofont-edit-alt  texto-amarelo"></i></a>
                <a href='#' data-id="<?= $admin['id'] ?>" class="removerAdmin texto-amarelo"><i class="icofont-ui-delete"></i></a>
            </td>
        </tr>
    <? } ?>
</table>
<div class="row">
    <? if ($paginacao) { ?>
        <div class="col-12 col-sm-12 col-md-7 col-lg-5">
            <?= $paginacao; ?>
        </div>
    <? } ?>
    <div class="col-12 col-md-4">
        <span class="text-light">Total de Registros: </span><span class="badge badge-secondary total"> <?= $totalRegistros ?></span>
    </div>
</div>