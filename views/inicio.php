<?
$dashboard = new Dashboard();
$moradoresByCond = $dashboard->getMoradoresbyCondominio();
$lastAdmins = $dashboard->lastAdmins();
$contTudo = $dashboard->getEveryCount()['resultSet'];
?>
<div class="container">
    <div class="row">
        <div class="col-12" style="align-items:center; justify-content:center;">
            <table class="table text-center table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th class="cse">Nome do Condominio</th>
                        <th>Quantidade de Moradores</th>
                    </tr>
                </thead>
                <? foreach ($moradoresByCond['resultSet']  as $dado) { ?>
                    <tr>
                        <td><?= $dado['nomeCondo'] ?></td>
                        <td><?= $dado['qt_moradores'] ?></td>
                    </tr>
                <? } ?>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <ul class="list-group text-center">
                <li class="list-group-item amarelo">Nome da Administradora</li>
                <? foreach ($lastAdmins['resultSet']  as $dado) { ?>
                    <li class="list-group-item"><?= $dado['nomeAdmin'] ?></li>
                <? } ?>
            </ul>
        </div>
    </div>
    <div class="row my-5  text-center">
        <div class="col-12 col-md-6 col-lg-3">
            <h1 class=""><span class="badge badge-secondary"><?=$contTudo['qtAdmin']?></span></h1>
            <h3 class="texto-branco">Administradoras</h2>
            <a href="<?= $url_site?>listaAdmin" class="texto-amarelo">Veja-as</a>
        </div>
        <div class="col-12 col-md-6 col-lg-2">
            <h1 class=""><span class="badge badge-secondary"><?=$contTudo['qtCondominio']?></span></h1>
            <h3 class="texto-branco">Condomínios</h2>
            <a href="<?= $url_site?>listaCondo" class="texto-amarelo">Veja-os</a>
        </div>
        <div class="col-12 col-md-6 col-lg-2">
            <h1 class=""><span class="badge badge-secondary"><?=$contTudo['qtBloco']?></span></h1>
            <h3 class="texto-branco">Blocos</h2>
            <a href="<?= $url_site?>listaBloco" class="texto-amarelo">Veja-os</a>
        </div>
        <div class="col-12 col-md-6 col-lg-2">
            <h1 class=""><span class="badge badge-secondary"><?=$contTudo['qtUnidade']?></span></h1>
            <h3 class="texto-branco">Unidades</h2>
            <a href="<?= $url_site?>listaUni" class="texto-amarelo">Veja-as</a>
        </div>
        <div class="col-12 col-md-6 col-lg-3">
            <h1 class=""><span class="badge badge-secondary"><?=$contTudo['qtMoradores']?></span></h1>
            <h3 class="texto-branco">Moradores</h2>
            <a href="<?= $url_site?>listaMorador" class="texto-amarelo">Veja-os</a>
        </div>
    </div>
    </div>
</div>
