<div class="container">
    <form action="" method="post" id='formCondo' class='mt-3'>
        <div class="form-group">
            <div class="input-group">
                <div class='col-12 col-md-6'>
                    <input type="text" class="form-control" name="nomeCondo" aria-describedby="required" value='<?= $popular['nomeCondo'] ?>' placeholder="Nome do Condomínio" required>
                </div>
                <div class='col-12 col-md-6'>
                    <input type="number" class="form-control" name="qtBloco" aria-describedby="required" value='<?= $popular['qtBloco'] ?>' placeholder="Quantidade de Blocos" required>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="col-12 col-md-6">
                    <input type="text" class="form-control" name="logra" aria-describedby="required" value='<?= $popular['logra'] ?>' placeholder="Logradouro" required>
                </div>
                <div class="col-12 col-md-6">
                    <input type="number" class="form-control" name="numEnd" aria-describedby="required" value='<?= $popular['numEnd'] ?>' placeholder="Número" required>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="col-12 col-md-6">
                    <input type="text" class="form-control" name="bairro" aria-describedby="required" value='<?= $popular['bairro'] ?>' placeholder="Bairro" required>
                </div>
                <div class="col-12 col-md-6">
                    <input type="text" class="form-control" name="cidade" aria-describedby="required" value='<?= $popular['cidade'] ?>' placeholder="Cidade" required>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="col-12 col-md-6">
                    <select name="estado" class="custom-select form-control">
                        <option selected>Estados</option>
                        <? foreach ($estados as $ch => $nome) { ?>
                            <option value="<?= $ch ?>" <?= ($ch == $popular['estado'] ? 'selected="selected"' : '') ?>><?= $nome ?></option>
                        <? } ?>
                    </select>
                </div>
                <div class="col-12 col-md-6">
                    <input type="number" class="form-control" name="cep" aria-describedby="required" value='<?= $popular['cep'] ?>' placeholder="CEP" required>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="col-12">
                <select name="from_admin" class="custom-select form-control">
                        <option selected>Administradoras</option>
                        <?foreach ($admin as $nome) { ?>
                            <option value="<?= $nome['id'] ?>" <?= ($nome['nomeAdmin'] == $popular['nomeAdmin'] ? 'selected' : '') ?>><?= $nome['nomeAdmin'] ?></option>
                        <? } ?> 
                    </select>
                </div>
            </div>
        </div>
        <? if ($_GET['id']) { ?>
            <input type="hidden" name='editar' value="<?= $_GET['id'] ?>">
        <? } ?>
        <button type="submit" class="btn btn-primary amarelo texto-preto buttonEnviar">ENVIAR</button>
    </form>
</div>