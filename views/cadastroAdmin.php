<div class="container">
    <form action="" method="post" id='formAdmin' class='mt-3' autocomple="off">
        <div class="form-group">
            <div class='col-12'>
                <input type="text" class="form-control" name="nomeAdmin" aria-describedby="required" value='<?= $popular['nomeAdmin'] ?>' placeholder="Nome da Administradora" required autocomple="off">
            </div>
        </div>
        <div class="form-group">
            <div class='col-12'>
                <input type="number" class="form-control" name="cnpj" aria-describedby="required" value='<?= $popular['cnpj'] ?>' placeholder="CNPJ" required autocomple="off"  >
            </div>
        </div>
        <? if ($_GET['id']) { ?>
            <input type="hidden" name='editar' value="<?= $_GET['id'] ?>">
        <? } ?>
        <button type="submit" class="btn btn-primary amarelo texto-preto buttonEnviar">ENVIAR</button>
    </form>
</div>