<div class="container">
    <form action="" method="post" id='formConFis' class='mt-3'>
        <div class="form-group">
            <select name="from_condominio" class="custom-select" required>
                <option value="">Selecione Condomínio</option>
                <?foreach ($condominio->getCondo()['resultSet'] as $condo) { ?>
                    <option value="<?= $condo['id'] ?>" <?= ($condo['nomeCondo'] == $popular['nomeCondo'] ? 'selected="selected"' : '') ?>><?= $condo['nomeCondo'] ?></option>
                <?}?>
            </select>
        </div>

        <div class="form-group">
            <select name="funcao" class="custom-select" required>
                <option value="">Selecione Funcao</option>
                    <option value="Síndico"<?= (isset($_GET['id'])) ? ($confis['funcao'] == $popular['funcao'] ? 'selected="selected"' : '') : ''?>>Síndico
                    </option>
                    <option value="Subsíndico" <?= (isset($_GET['id'])) ? ($confis['funcao'] == $popular['funcao'] ? 'selected="selected"' : '') : '' ?>>Sub-Síndico</option>
                    <option value = "Conselheiro" <?= (isset($_GET['id'])) ? ($confis['funcao'] == $popular['funcao'] ? 'selected="selected"' : '') : '' ?>>Conselheiro</option>
            </select>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="nome" aria-describedby="required" value='<?= $popular['nome'] ?>' placeholder="Nome do Funcionário do Conselho" required>
        </div>
        <? if ($_GET['id']) { ?>
            <input type="hidden" name="editar" value="<?= $_GET['id'] ?>">
        <? } ?>
        <button type="submit" class="btn btn-primary amarelo texto-preto buttonEnviar">ENVIAR</button>
    </form>
</div>