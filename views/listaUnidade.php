<table class='table text-secondary table-hover text-center table-responsive-lg'>
        <tr class="">
            <td colspan=6 class='align-middle'>
            <form method="GET" id="filtro" class="form-inline mx-2 my-2 my-lg-0">
                <input type="hidden" name="page" value="listaUnidade">
                <div class="input-group-prepend">
                    <span class="input-group-text">Por Número</span>
                    <input type="search" class="form-control mr-sm-2 termo1 mb-auto" name="b[numUni]" placeholder="Unidade">
                </div>
                <button class="btn btn-outline-success mx-2 my-2 my-sm-0 buscar" type="submit" disabled>Buscar</button>
                <a href="<?= $url_site ?>listaUnidade" class="btn btn-outline-danger">LIMPAR BUSCA</a>
            </form>
            </td>
            <td colspan=2 class='align-middle'><a href="<?= $url_site?>cadastroUnidade" class="text-light btn btn-primary amarelo" style='width: 100%;'>Adicionar <i class="icofont-ui-add"></i></a></td>
        </tr>
        <tr class='thead-dark'>
            <th class='align-middle'>Nome do Condominio</th>
            <th class='align-middle'>Nome do Bloco</th>
            <th class='align-middle'>Número da Unidade</th>
            <th class='align-middle'>Metragem da Unidade</th>
            <th class='align-middle'>Quantidade de Vagas de Garagem</th>
            <th class='align-middle'>Criação:</th>
            <th class='align-middle'>Atualização:</th>
            <th class='align-middle'></th>
        </tr>
        <?foreach($result['resultSet'] as $uni) {?>
            <tr class='text-center flex-wrap' data-id='<?=$uni['id']?>'>
                <td class='align-middle'><?= $uni['nomeCondo'] ?></td>
                <td class='align-middle'><?= $uni['nomeBloco'] ?></td>
                <td class='align-middle'><?= $uni['numUni'] ?></td>
                <td class='align-middle'><?= $uni['metraUni'] ?></td>
                <td class='align-middle'><?= $uni['vagasUni'] ?></td>
                <td class='align-middle'><?=dateFormat($uni['dataCadastro'])?></td>
                <td class='align-middle'><?=dateFormat($uni['dataUpdate'])?></td>
                <td class='align-middle'><a href='<?=$url_site?>cadastroUnidad/id/<?=$uni['id']?>' class="texto-amarelo"><i class="icofont-edit-alt"></i></a>
                <a href='#' data-id="<?=$uni['id']?>" class='removerUnidade texto-amarelo'><i class="icofont-ui-delete"></i></a>
            </td>
            </tr>
        <?}?>
</table>
<div class="col-sm-12">
    <div class="row">
        <? if ($paginacao) { ?>
            <div class="col-12 col-sm-12 col-md-7 col-lg-5">
                <div><?= $paginacao ?></div>
            </div>
        <? } ?>
        <div class="col-md-4">
            <span class="text-light">Total de Registros: <span class="badge badge-secondary total"><?= $totalRegistros ?></span></span>
        </div>
    </div>
</div>
