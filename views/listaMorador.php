<div class="row">
    <div class="col-12">
        <table class='table table-hover text-center table-responsive-xl' width="100%">
            <tr>
                <td colspan=8>
                    <form class="form-inline mx-2 my-2 my-lg-0" method="GET" id="filtro">
                        <input type="hidden" name="page" value="listaMorador">
                        <div class="input-group-prepend">
                            <div class="input-group-text">Por nome</div>
                            <input class="form-control mr-sm-2 mb-auto termo1" type="search" placeholder="Buscar por nome" aria-label="Search" name="b[nome]">
                        </div>

                        <div class="input-group-prepend">
                            <div class="input-group-text">Por Condominio</div>
                            <select name="b[from_condominio]" class="custom-select termo2" id="">
                                <option value="">...</option>
                                <? foreach ($listCondominio['resultSet'] as $condominios) { ?>
                                    <option value="<?= $condominios['id'] ?>"><?= $condominios['nomeCondo'] ?></option>;
                                <? } ?>
                            </select>
                        </div>
                        <button class="btn btn-outline-success mx-2 my-2 my-sm-0 buscar" type="submit" disabled>Buscar</button>

                        <a href="<?= $url_site ?>listaMorador" class="btn btn-outline-danger">LIMPAR BUSCA</a>
                    </form>
                </td>
                <td colspan=2>
                    <a href="<?= $url_site ?>cadastroMorador" class="text-light btn btn-primary amarelo" style='width: 100%;'>Adicionar <i class="icofont-ui-add"></i></a>
                </td>
            </tr>
            <tr class='thead-dark'>
                <th class='align-middle'>Nome do Condominio</th>
                <th class='align-middle'>Nome Bloco</th>
                <th class='align-middle'>Num. Unidade</th>
                <th class='align-middle'>Nome</th>
                <th class='align-middle'>CPF</th>
                <th class='align-middle'>E-mail</th>
                <th class='align-middle'>Telefone</th>
                <th class='align-middle'>Criação:</th>
                <th class='align-middle'>Atualização:</th>
                <th class='align-middle'></th>
            </tr>
            <? foreach ($result['resultSet'] as $cliente) { ?>
                <tr class='text-center flex-wrap' data-id='<?= $cliente['id'] ?>'>
                    <td class='align-middle'><?= $cliente['nomeCondo'] ?></td>
                    <td class='align-middle'><?= $cliente['nomeBloco'] ?></td>
                    <td class='align-middle'><?= $cliente['numUni'] ?></td>
                    <td class='align-middle'><?= $cliente['nome'] ?></td>
                    <td class='align-middle'><?= $cliente['cpf'] ?></td>
                    <td class='align-middle'><?= $cliente['email'] ?></td>
                    <td class='align-middle'><?= $cliente['telefone'] ?></td>
                    <td class='align-middle'><?= dateFormat($cliente['dataCadastro']) ?></td>
                    <td class='align-middle'><?= dateFormat($cliente['dataUpdate']) ?></td>
                    <td class='align-middle'>
                        <a href='<?= $url_site ?>cadastroMorador/id/<?= $cliente['id'] ?>' class="texto-amarelo"><i class="icofont-edit-alt"></i></a>
                        <a href='#' data-id="<?= $cliente['id'] ?>" class='removerMorador texto-amarelo'><i class="icofont-ui-delete"></i></a>
                    </td>
                </tr>
            <? } ?>
        </table>

    </div>
</div>
<div class="row">
    <? if ($paginacao) { ?>
        <div class="col-12 col-sm-12 col-md-7 col-lg-5">
            <div><?= $paginacao ?></div>
        </div>
    <? } ?>
    <div class="col-md-2">
        <span class="text-light">Total de Registros: <span class="badge badge-secondary total"><?= $totalRegistros ?></span></span>
    </div>
</div>