<div class="container">
    <form action="" method="post" id='formUnidade' class='mt-3'>
        <div class="form-group">
            <select name="from_condominio" class="custom-select fromCondominio" required>
                <option value="">Selecione...</option>
                <?foreach ($condos as $condo) { ?>
                    <option value="<?= $condo['id'] ?>" <?= ($condo['nomeCondo'] == $popular['nomeCondo'] ? 'selected="selected"' : '') ?>><?= $condo['nomeCondo'] ?></option>
                <? } ?>
            </select>
        </div>
        <div class="form-group">
            <select name="from_bloco" class="custom-select fromBloco" required>
                <? foreach ($blocos['resultSet'] as $bloco) { ?>
                        <option value="<?= $bloco['id'] ?>" <?= ($bloco['id'] == $popular['from_bloco'] ? 'selected' : '') ?>><?= $bloco['nomeBloco'] ?></option>
                <? } ?>
            </select>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="numUni" aria-describedby="required" value='<?= $popular['numUni'] ?>' placeholder="Número da Unidade" required>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="metraUni" aria-describedby="required" value='<?= $popular['metraUni'] ?>' placeholder="Metragem de Unidade" required>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="vagasUni" aria-describedby="required" value='<?= $popular['vagasUni'] ?>' placeholder="Vagas de Estacionamento da Unidade" required>
        </div>
        <? if ($_GET['id']) { ?>
            <input type="hidden" name='editar' value="<?= $_GET['id'] ?>">
        <? } ?>
        <button type="submit" class="btn btn-primary amarelo texto-preto buttonEnviar">ENVIAR</button>
    </form>
</div>