<div class="container">
    <form action="" method="post" id='formCliente' class='mt-3'>
        <div class="form-group">
            <div class="input-group">
                <div class="col-4">
                    <select name="from_condominio" class="custom-select fromCondominio" required>
                        <option value="">Condominios</option>
                        <?foreach ($condos as $condo) {?>
                            <option value="<?= $condo['id'] ?>" <?= ($condo['id'] == $popular['from_condominio'] ? 'selected="selected"' : '') ?>><?= $condo['nomeCondo'] ?></option>
                        <? } ?>
                    </select>
                    <small id='required' class="form-text text-danger">Esse campo é obrigatório</small>
                </div>
                <div class="col-4">
                    <select name="from_bloco" class="custom-select fromBloco" required>
                        <?foreach($blocos['resultSet'] as $bloco) {?>
                            <option value = "<?=$bloco['id']?>" <?=($bloco['id'] == $popular['from_bloco'] ? 'selected' : '')?>><?=$bloco['nomeBloco']?></option>
                        <?}?>
                    </select>
                    <small id='required' class="form-text text-danger">Esse campo é obrigatório</small>
                </div>
                <div class="col-4">
                    <select name="from_unidade" class="custom-select fromUnidade" required>
                    <?foreach($unis['resultSet'] as $uni) {?>
                            <option value = "<?=$uni['id']?>"><?=$uni['numUni']?></option>
                    <?}?>
                    </select>
                    <small id='required' class="form-text text-danger">Esse campo é obrigatório</small>
                </div>
            </div>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="nome" aria-describedby="required" value='<?= $popular['nome'] ?>' placeholder="Nome" required>
            <small id='required' class="form-text text-danger">Esse campo é obrigatório</small>
        </div>
        <div class="form-group">
            <input type="email" class="form-control" name="email" aria-describedby="required" value='<?= $popular['email'] ?>' placeholder="E-mail" required>
            <small id='required' class="form-text text-danger">Esse campo é obrigatório</small>
        </div>
        <div class="form-group">
            <input type="password" class="form-control" name="senha" aria-describedby="required" value='<?= $popular['senha'] ?>' placeholder="Senha" required>
            <small id='required' class="form-text text-danger">Esse campo é obrigatório</small>
        </div>
        <div class="form-group">
            <input type="text" class="form-control cpf" name="cpf" aria-describedby="required" value='<?= $popular['cpf'] ?>' placeholder="CPF" required>
            <small id='required' class="form-text text-danger">Esse campo é obrigatório</small>
        </div>
        <? if ($_GET['id']) { ?>
            <input type="hidden" name='editar' value="<?= $_GET['id'] ?>">
        <? } ?>
        <div class="form-group">
            <input type="text" class="form-control" name="telefone" aria-describedby="required" value='<?= $popular['telefone'] ?>' placeholder="Telefone">
        </div>
        <button type="submit" class="btn btn-primary amarelo texto-preto buttonEnviar">ENVIAR</button>
    </form>
</div>