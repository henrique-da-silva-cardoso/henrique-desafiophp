<?
include 'funcoes.php';
include 'controllers/nav.php';
$user = new Restrito();
if ($_GET['page'] == "logout") {
    if ($user->logout()) {
        header('Location: ' . $url_site . 'login.php');
    }
}
if (!$user->acesso()) {
    header('Location:' . $url_site . 'login.php');
}
// session_destroy();
//array que gerará a nav

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?= $url_site ?>css/main.css">
    <link rel="stylesheet" href="<?= $url_site ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= $url_site ?>icofont/icofont.min.css">
    <link rel="icon" href="<?= $url_site ?>imgs/apcontrole.png" class="icone">
    <title>AP Controle</title>
</head>

<body>
    <div class="container-fluid bg-dark mb-3 text-light">
        <nav class="navbar navbar-expand-lg pl-0">
        <a class="nav-link active text-light" href="<?= $url_site ?>inicio">Página Inicial</a>
            <button class="navbar-toggler texto-branco" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="pt-1 navbar-toggler-icon texto-branco"><i class="icofont-navigation-menu"></i></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <? foreach ($nav as $nomeSessoes => $sessoes) { ?>
                        <li class="pl-3 nav-item">
                            <div class="dropdown show">
                                <a class="dropdown-toggle text-light nav-link active" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?= $nomeSessoes ?>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <? foreach ($sessoes as $ch => $d) { ?>
                                        <a class="dropdown-item" href="<?= $url_site . $ch ?>"><?= $d ?></a>
                                    <? } ?>
                                </div>
                            </div>
                        </li>
                    <? } ?>
                </ul>
                <ul class="nav">
                    <?
                    $nome = explode(' ', $_SESSION['USUARIO']['nome']);
                    ?>
                    <li class="nav-item "><span class="nav-link">Olá <?= $nome[0]; ?>, seja bem vinde</span></li>
                    <li class="nav-item config ml-auto"><a href="<?= $url_site ?>logout" class="nav-link text-light  "><i class="icofont-logout"></i></a></li>
                </ul>
            </div>
        </nav>
    </div>
    <main>
        <div class="container">
            <?
            switch ($_GET['page']) {
                default:
                    require 'controllers/' . $_GET['page'] . '.php';
                    require 'views/' . $_GET['page'] . '.php';
                    break;
            }
            ?>
        </div>
    </main>
    <footer class='container-fluid bg-dark text-light'>
        <div class="row bg-dark mt-2">
            <div class="col-12 col-md-4 text-center">Gestor de Condomínios 2022</div>
            <div class="col-12 col-md-4 text-center">Blumenau, <?= date("d") ?> de <?= date("m") ?> de <?= date('Y') ?></div>
            <div class="col-12 col-md-4 text-center"><i class="icofont-brand-whatsapp"></i> Suporte</div>
        </div>
        </footer>
    <script>
        var url_site = '<?= $url_site ?>';
    </script>
    <script src="<?= $url_site ?>js/jquery-3.6.0.min.js"></script>
    <script src="<?= $url_site ?>js/bootstrap.bundle.min.js"></script>
    <!-- <script src="<?= $url_site ?>js/jquery.mask.min.js"></script> -->
    <script src="<?= $url_site ?>js/app.js?v=" <? rand(0, 9999) ?>></script>
</body>

</html>