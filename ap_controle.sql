-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.22-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para ap_controle
DROP DATABASE IF EXISTS `ap_controle`;
CREATE DATABASE IF NOT EXISTS `ap_controle` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `ap_controle`;

-- Copiando estrutura para tabela ap_controle.administradoras
DROP TABLE IF EXISTS `administradoras`;
CREATE TABLE IF NOT EXISTS `administradoras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeAdmin` varchar(255) NOT NULL,
  `cnpj` varchar(14) NOT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT concat(curdate(),' ',curtime()),
  `dataUpdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.administradoras: ~4 rows (aproximadamente)
DELETE FROM `administradoras`;
/*!40000 ALTER TABLE `administradoras` DISABLE KEYS */;
INSERT INTO `administradoras` (`id`, `nomeAdmin`, `cnpj`, `dataCadastro`, `dataUpdate`) VALUES
	(1, 'Grupo Lima Barreto', '93249832000127', '2022-04-01 09:46:02', '2022-04-01 11:29:42'),
	(2, 'Grupo Caetano Veloso', '98268768000145', '2022-04-01 09:46:02', '2022-04-01 09:47:41'),
	(3, 'Grupo Alex Atala', '18381901000136', '2022-04-01 09:46:02', '2022-04-01 09:47:41'),
	(13, 'Grupo Jonathan James', '93839104859294', '2022-04-06 09:06:29', '2022-04-06 09:06:29');
/*!40000 ALTER TABLE `administradoras` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.blocos
DROP TABLE IF EXISTS `blocos`;
CREATE TABLE IF NOT EXISTS `blocos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_condominio` int(11) NOT NULL,
  `nomeBloco` varchar(255) NOT NULL,
  `qtAndares` int(11) NOT NULL,
  `qtUnidades` int(11) NOT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT concat(curdate(),' ',curtime()),
  `dataUpdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK_bloco_condominio` (`from_condominio`),
  CONSTRAINT `FK_bloco_condominio` FOREIGN KEY (`from_condominio`) REFERENCES `condominios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.blocos: ~24 rows (aproximadamente)
DELETE FROM `blocos`;
/*!40000 ALTER TABLE `blocos` DISABLE KEYS */;
INSERT INTO `blocos` (`id`, `from_condominio`, `nomeBloco`, `qtAndares`, `qtUnidades`, `dataCadastro`, `dataUpdate`) VALUES
	(1, 1, 'A', 4, 4, '2022-03-31 16:47:23', '2022-04-01 11:30:18'),
	(2, 2, 'A', 5, 5, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(3, 3, 'A', 2, 6, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(4, 4, 'A', 1, 10, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(5, 5, 'A', 32, 10, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(6, 6, 'A', 4, 10, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(7, 7, 'A', 7, 2, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(8, 8, 'A', 8, 3, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(9, 9, 'A', 5, 1, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(10, 10, 'A', 6, 9, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(11, 11, 'A', 2, 10, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(12, 12, 'A', 6, 5, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(14, 1, 'B', 4, 1, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(15, 2, 'B', 5, 2, '2022-03-31 16:47:23', '2022-04-01 11:27:13'),
	(16, 3, 'B', 6, 3, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(17, 4, 'B', 3, 5, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(18, 5, 'B', 2, 4, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(19, 6, 'B', 8, 9, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(20, 7, 'B', 7, 8, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(21, 8, 'B', 9, 6, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(22, 9, 'B', 10, 5, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(23, 10, 'B', 6, 4, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(24, 11, 'B', 7, 3, '2022-03-31 16:47:23', '2022-03-31 17:13:34'),
	(25, 12, 'B', 8, 2, '2022-03-31 16:47:23', '2022-03-31 17:13:34');
/*!40000 ALTER TABLE `blocos` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.condominios
DROP TABLE IF EXISTS `condominios`;
CREATE TABLE IF NOT EXISTS `condominios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeCondo` varchar(255) NOT NULL,
  `qtBloco` int(11) NOT NULL,
  `logra` varchar(255) NOT NULL,
  `numEnd` varchar(8) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `estado` varchar(2) NOT NULL,
  `cep` varchar(8) NOT NULL,
  `from_admin` int(11) NOT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT concat(curdate(),' ',curtime()),
  `dataUpdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK_condominio_aministradora` (`from_admin`),
  CONSTRAINT `FK_condominio_aministradora` FOREIGN KEY (`from_admin`) REFERENCES `administradoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.condominios: ~13 rows (aproximadamente)
DELETE FROM `condominios`;
/*!40000 ALTER TABLE `condominios` DISABLE KEYS */;
INSERT INTO `condominios` (`id`, `nomeCondo`, `qtBloco`, `logra`, `numEnd`, `bairro`, `cidade`, `estado`, `cep`, `from_admin`, `dataCadastro`, `dataUpdate`) VALUES
	(1, 'Aurora Borealis', 4, 'Rua Luiz Eleodoro da Silva', '387', 'Ponta Aguda', 'Blumenau', 'AM', '12345678', 1, '2022-03-31 16:55:31', '2022-03-31 16:55:31'),
	(2, 'Facão', 6, 'Rua Vinte Pra Nove', '342', 'Dardo Pontiagudo', 'Redmenau', 'AP', '23456789', 2, '2022-03-31 16:53:46', '2022-03-31 16:51:33'),
	(3, 'Minério Suíço', 3, 'Rua Maré Agitada', '932', 'Nome Pendente', 'Yellowmenau', 'MT', '18273645', 3, '2022-03-31 16:55:57', '2022-03-31 16:55:57'),
	(4, 'Sapato Famoso', 7, 'Rua Dídimo de Souza Santos', '436', 'Núcleo Residencial Tutunas', 'Uberaba', 'MG', '38057430', 3, '2022-03-30 13:20:25', '2022-03-31 16:56:56'),
	(5, 'Ponto Congelado', 10, 'Rua Vento Levou', '213', 'Jericho dos Santos', 'Purplemenau', 'SP', '90220030', 2, '2022-03-30 13:12:29', '2022-03-31 16:51:33'),
	(6, 'Eco Marcado', 5, 'Rua Pedra Mole', '734', 'Bahia dos Navegadores', 'Cianmenau', 'AL', '49000492', 2, '2022-03-30 13:12:35', '2022-03-31 16:51:33'),
	(7, 'Trincheira de Motorista', 8, 'Rua dos Capuchinhos', '532', 'Saguaçu', 'Joinville', 'SC', '89221210', 1, '2022-03-30 13:12:44', '2022-03-31 16:51:33'),
	(8, 'Dois Rebites', 4, '1ª Travessa Beira Mar', '824', 'Igapó', 'Natal', 'RN', '59101066', 2, '2022-03-30 13:13:38', '2022-03-31 16:51:33'),
	(9, 'Observando Trança', 5, 'Rua Bororós', '019', 'Vila Tupã Mirim I', 'Tupã', 'SP', '17603030', 1, '2022-03-30 13:17:56', '2022-03-31 16:51:33'),
	(10, 'Catapulta de Fogão', 2, 'Rua Cassiano Ricardo', '853', 'Caravágio', 'Lages', 'SC', '88509435', 3, '2022-03-30 13:18:04', '2022-03-31 16:51:33'),
	(11, 'Lixeira Esgrimista', 14, 'Rua Erli Cabral de Lima', '289', 'Gramame', 'João Pessoa', 'PB', '58068388', 2, '2022-03-30 13:18:07', '2022-03-31 16:51:33'),
	(12, 'Guaxinim Desajeitado', 3, 'Rua Gererê', '192', 'Merenda', 'Maranhão', 'Es', '93958038', 3, '2022-03-31 14:45:54', '2022-04-06 14:29:35'),
	(44, '123', 12, '123', '123', '1231', '123', 'AM', '123', 13, '2022-04-06 14:53:18', '2022-04-06 14:53:18');
/*!40000 ALTER TABLE `condominios` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.confis
DROP TABLE IF EXISTS `confis`;
CREATE TABLE IF NOT EXISTS `confis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_condominio` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `funcao` enum('Síndico','Subsíndico','Conselheiro') NOT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT concat(curdate(),' ',curtime()),
  `dataUpdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK_confis_condominio` (`from_condominio`),
  CONSTRAINT `FK_confis_condominio` FOREIGN KEY (`from_condominio`) REFERENCES `condominios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.confis: ~60 rows (aproximadamente)
DELETE FROM `confis`;
/*!40000 ALTER TABLE `confis` DISABLE KEYS */;
INSERT INTO `confis` (`id`, `from_condominio`, `nome`, `funcao`, `dataCadastro`, `dataUpdate`) VALUES
	(1, 1, 'Manuel de Oliveira', 'Síndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(2, 1, 'José Pires Bezerra', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(3, 1, 'Hernâni Cedro Sardinha', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(4, 1, 'Flor Cavalheiro Paulos', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(5, 2, 'Nádia Pontes Guerreiro', 'Síndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(6, 2, 'Teotónio Remígio Cabreira', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(7, 2, 'Jessica Foquiço Vilariça\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(8, 2, 'Raúl Pádua Holanda\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(9, 3, 'Verónica Mourinho Fiúza\r\n', 'Síndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(10, 3, 'Mafalda Rico Vilhena\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(11, 3, 'Jussara Morão Delgado\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(12, 3, 'Nour Redondo Naves\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(13, 4, 'Estêvão Guedelha França\r\n', 'Síndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(14, 4, 'Chloé Madeira Bettencourt\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(15, 4, 'Francesco Rego Mesquita\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(16, 4, 'Helder Picanço Quintela\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(17, 5, 'Luis Pureza Amarante\r\n', 'Síndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(18, 5, 'Amina Simão Calado\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(19, 5, 'Hannah Teixeira Balsemão\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(20, 5, 'Antônio Frazão Mirandela\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(21, 6, 'Ahmad Castilho Quental\r\n', 'Síndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(22, 6, 'Ashley Marques Sacadura\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(23, 6, 'Lígia Piteira Proença\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(24, 6, 'Albert Rico Dâmaso\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(25, 7, 'Yi Valadim Freitas\r\n', 'Síndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(26, 7, 'Eric Camargo Sabala\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(27, 7, 'Yaqi Rufino Furquim\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(28, 7, 'Ariel Serpa Carromeu\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(29, 8, 'Bela Carrasqueira Fortunato\r\n', 'Síndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(30, 8, 'Rubim Tristão Cachão\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(31, 8, 'India Casqueira Caneira\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(32, 8, 'Kamila Cerveira Lamego\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(33, 9, 'Carlos Gil Cavaco\r\n', 'Síndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(34, 9, 'Liliana Granjeiro Malho\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(35, 9, 'Aryan Canejo Paranhos\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(36, 9, 'Cristóvão Garcia Ferro\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(37, 10, 'Grace Gonçalves Quental\r\n', 'Síndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(38, 10, 'Joel Miranda Cotrim\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(39, 10, 'Pietra Malta Borges\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(40, 10, 'Nicola Castanho Freixo\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(41, 11, 'Emilie Azevedo Natal\r\n', 'Síndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(42, 11, 'Ndeye Neres Camacho\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(43, 11, 'Isaac França Monforte\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(44, 11, 'Jason Linhares Rebelo\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(45, 12, 'Jussara Basílio Félix\r\n', 'Síndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(46, 12, 'Brayan Vigário Talhão\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(47, 12, 'Daisi Rebimbas Roriz\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(48, 12, 'Leonardo Silvestre Escobar\r\n', 'Conselheiro', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(49, 1, 'Renato Paixão Dâmaso\r\n', 'Subsíndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(50, 2, 'Yi Xavier Raposo\r\n', 'Subsíndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(51, 3, 'Shaira Cortês Quintão\r\n', 'Subsíndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(52, 4, 'Amélia Vergueiro Cirne\r\n', 'Subsíndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(53, 5, 'Ayaan Vidal Barrela\r\n', 'Subsíndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(54, 6, 'Américo Barreno Frazão\r\n', 'Subsíndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(55, 7, 'Angela Cabral Damasceno\r\n', 'Subsíndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(56, 8, 'Maísa Ataíde Festas\r\n', 'Subsíndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(57, 9, 'Raphaël Foquiço Mourato\r\n', 'Subsíndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(58, 10, 'Thayra Bernardes Figueiredo\r\n', 'Subsíndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(59, 11, 'Luena Alcântara Homem\r\n', 'Subsíndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24'),
	(60, 12, 'Lara Camarinho Quirino\r\n', 'Subsíndico', '2022-04-06 15:22:31', '2022-04-06 15:23:24');
/*!40000 ALTER TABLE `confis` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.lista_convidados
DROP TABLE IF EXISTS `lista_convidados`;
CREATE TABLE IF NOT EXISTS `lista_convidados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `convidado` varchar(50) NOT NULL,
  `cpf` varchar(11) NOT NULL DEFAULT '',
  `celular` varchar(255) NOT NULL,
  `from_reserva_salao_festas` int(11) NOT NULL,
  `from_unidade` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_lista_convidados_reserva_salao_festas` (`from_reserva_salao_festas`),
  KEY `FK_lista_convidados_unidade` (`from_unidade`),
  CONSTRAINT `FK_lista_convidados_reserva_salao_festas` FOREIGN KEY (`from_reserva_salao_festas`) REFERENCES `reserva_salao_festas` (`id`),
  CONSTRAINT `FK_lista_convidados_unidade` FOREIGN KEY (`from_unidade`) REFERENCES `unidades` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.lista_convidados: ~24 rows (aproximadamente)
DELETE FROM `lista_convidados`;
/*!40000 ALTER TABLE `lista_convidados` DISABLE KEYS */;
INSERT INTO `lista_convidados` (`id`, `convidado`, `cpf`, `celular`, `from_reserva_salao_festas`, `from_unidade`) VALUES
	(1, 'Alana Vanessa Esther da Cunha', '21148319654', '(21) 98242-2330', 1, 5),
	(2, 'Melissa Luzia Carla Moraes', '67780246078', '(92) 99531-3994', 1, 4),
	(3, 'Emily Kamilly Araújo', '16538464025', '(49) 98915-1357', 1, 7),
	(4, 'Ryan Luiz Luiz Sales', '03111263916', '849.277.595-55', 1, 9),
	(5, 'Laís Marina Ferreira', '45788197600', '(95) 99275-1367', 1, 7),
	(6, 'Igor Matheus Thiago da Mata', '63008441378', '(27) 96817-9516', 1, 6),
	(7, 'Agatha Elza Aparecida Moura', '40941611477', '(67) 98219-1824', 1, 3),
	(8, 'Elaine Laura Costa', '88226765560', '(84) 98414-4443', 1, 2),
	(9, 'Raquel Larissa Regina BaptistaAlana Vanessa Esther', '13849960870', '(84) 97335-0264', 2, 9),
	(10, 'Jonathan Sacadura Valcanaia', '67135705009', '(61) 97174-5725', 2, 8),
	(11, 'Tyler Pardo Viana', '40591052016', '(53) 98271-4357', 2, 5),
	(12, 'Aléxia Vital Monjardim', '31965806015', '(66) 98138-5822', 2, 7),
	(13, 'Janaína Fonseca Murtinho', '40554379007', '(81) 98828-1575', 2, 4),
	(14, 'Aléxis Camacho Cordeiro', '78144295013', '(68) 99382-7745', 2, 2),
	(15, 'Alexandre Paiva Sandinha', '62942539003', '(15) 97355-3451', 2, 1),
	(16, 'Tyron Cerveira Colaço\r\n', '47769187060', '(62) 98956-1785', 2, 6),
	(17, 'Ruben Taveiros Quintanilha\r\n', '04557194036', '(77) 99444-4983', 3, 6),
	(18, 'Vânia Coelho Hilário\r\n', '85673938098', '(88) 98437-6106', 3, 6),
	(19, 'Carolina Castelo Cruz\r\n', '51282349082', '(92) 98292-5670', 3, 6),
	(20, 'Albert Capucho Barcelos\r\n', '12959630009', '(65) 96922-2933', 3, 6),
	(21, 'Samaritana Olaio Velasques\r\n', '46264073075', '(88) 96928-4587', 3, 6),
	(22, 'Andreína Vasconcelos Rosa', '99569663049', '(28) 98811-3378', 3, 6),
	(23, 'Manuela Carromeu Lampreia', '12673736018', '(44) 99638-5326', 3, 6),
	(24, 'Jacyara Damásio Saloio', '65637539065', '(27) 98935-4641', 3, 6);
/*!40000 ALTER TABLE `lista_convidados` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.moradores
DROP TABLE IF EXISTS `moradores`;
CREATE TABLE IF NOT EXISTS `moradores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `cpf` varchar(255) NOT NULL,
  `telefone` varchar(255) NOT NULL,
  `from_condominio` int(11) NOT NULL,
  `from_bloco` int(11) NOT NULL,
  `from_unidade` int(11) NOT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT concat(curdate(),' ',curtime()),
  `dataUpdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK1` (`from_bloco`),
  KEY `FK2` (`from_condominio`),
  KEY `FK3` (`from_unidade`),
  CONSTRAINT `FK1` FOREIGN KEY (`from_bloco`) REFERENCES `blocos` (`id`),
  CONSTRAINT `FK2` FOREIGN KEY (`from_condominio`) REFERENCES `condominios` (`id`),
  CONSTRAINT `FK3` FOREIGN KEY (`from_unidade`) REFERENCES `unidades` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.moradores: ~17 rows (aproximadamente)
DELETE FROM `moradores`;
/*!40000 ALTER TABLE `moradores` DISABLE KEYS */;
INSERT INTO `moradores` (`id`, `nome`, `email`, `senha`, `cpf`, `telefone`, `from_condominio`, `from_bloco`, `from_unidade`, `dataCadastro`, `dataUpdate`) VALUES
	(1, 'Fernando', 'ca@gmail.com', '1020394', '22566675023', '1929301049', 1, 1, 1, '2022-04-01 14:08:11', '2022-04-01 14:09:47'),
	(2, 'Fernanda', 'ka@gmail.com', '39284229', '93875827015', '12312412', 2, 2, 2, '2022-04-01 14:08:11', '2022-04-01 14:09:47'),
	(3, 'Fernande', 'la@gmail.com', '10309494', '60273616030', '1929301049', 3, 3, 3, '2022-04-01 14:08:11', '2022-04-01 14:09:47'),
	(5, 'Manual', 'sfasdf@gmail.com', '1231221', '71571085009', '12312312312', 5, 5, 5, '2022-04-01 14:08:11', '2022-04-01 14:09:49'),
	(6, 'Alice Aline Bruna Moreira', 'alice_moreira@unitau.br', 'cHTBH0c6Tp', '19037555063', '(27) 2552-0403', 6, 6, 6, '2022-04-01 14:08:11', '2022-04-01 14:09:50'),
	(7, 'Benedito Caio Monteiro', 'benedito-monteiro81@duoarq.com', 'aGJ6LTJcoE', '28487599095', '(73) 3914-5146', 7, 7, 7, '2022-04-01 14:08:11', '2022-04-01 14:09:50'),
	(8, 'Jaqueline Mirella Marina Mendes', 'jaqueline.mirella.mendes@eccotrans.com.br', 'dBSmczPzrI', '01117793087', '(27) 3823-1947', 8, 8, 8, '2022-04-01 14:08:11', '2022-04-01 14:09:51'),
	(9, 'Cauã Lucca Oliveira', 'caua_lucca_oliveira@contabilidadevictoria.com.br', 'N9ItrqyDLS', '94172958028', '(86) 2975-5907', 9, 9, 9, '2022-04-01 14:08:11', '2022-04-01 14:09:51'),
	(10, 'Cláudio Márcio Baptista', 'claudio_marcio_baptista@bodyfast.com.br', 'KkKclsyMuM', '97290595009', '(86) 3986-4096', 10, 10, 10, '2022-04-01 14:08:11', '2022-04-01 14:09:52'),
	(11, 'Giovanni Gustavo Thales Alves', 'giovanni_gustavo_alves@msds.com.br', 'rKnL9WXW6u', '41842914065', '(61) 2554-9256', 11, 11, 11, '2022-04-01 14:08:11', '2022-04-01 14:09:52'),
	(12, 'Alana Vanessa Esther da Cunha', 'alana.vanessa.dacunha@editorazap.com.br', '5fXPgcepFt', '07297179094', '(21) 3753-6839', 12, 12, 12, '2022-04-01 14:08:11', '2022-04-05 11:01:02'),
	(13, 'Ruan Isaac Castro', 'ruan.isaac.castro@consorciobcv.com.br', 'zHsipJBWzF', '73982771056', '(85) 3590-0451', 1, 14, 1, '2022-04-01 14:08:11', '2022-04-01 14:09:53'),
	(14, 'Leonardo Kauê Nascimento', 'leonardo_nascimento@cielo.com.br', 'JncKqcziRQ', '65962962028', '(12) 2580-7997', 2, 15, 2, '2022-04-01 14:08:11', '2022-04-01 14:09:54'),
	(15, 'Carlos Marcos Kauê Campos', 'carlos-campos98@csjsistemas.com.br', 'ABh25ohPQ6', '47712704066', '(51) 2710-3311', 3, 16, 3, '2022-04-01 14:08:11', '2022-04-01 14:09:54'),
	(16, 'Cristiane Carla Sebastiana Mendes', 'cristiane-mendes70@outloock.com', 'kqBikxdbY6', '89509921084', '(79) 3521-7010', 4, 17, 4, '2022-04-01 14:08:11', '2022-04-01 14:09:56'),
	(39, 'Mariana das Fossas', 'deepestpartoftheocean@gmail.com', '123123123', '123123123', '1231123123', 10, 10, 10, '2022-04-05 11:14:49', '2022-04-05 11:14:49'),
	(40, 'Kaique Hugo Rodrigues', 'kaiquehugorodrigues@daou.com.br', '2gdKPQ3d1R', '72545782050', '9239836241', 3, 16, 17, '2022-04-06 14:44:30', '2022-04-06 14:44:30');
/*!40000 ALTER TABLE `moradores` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.pets
DROP TABLE IF EXISTS `pets`;
CREATE TABLE IF NOT EXISTS `pets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomepet` varchar(255) NOT NULL DEFAULT '',
  `tipo` enum('Cachorro','Gato','Passarinho') NOT NULL,
  `from_morador` int(11) NOT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK_pets_cadastro` (`from_morador`),
  CONSTRAINT `FK_pets_cadastro` FOREIGN KEY (`from_morador`) REFERENCES `moradores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.pets: ~2 rows (aproximadamente)
DELETE FROM `pets`;
/*!40000 ALTER TABLE `pets` DISABLE KEYS */;
INSERT INTO `pets` (`id`, `nomepet`, `tipo`, `from_morador`, `dataCadastro`) VALUES
	(1, 'Zeus', 'Cachorro', 1, '2022-03-30 11:45:18'),
	(2, 'Mara', 'Passarinho', 2, '2022-03-30 11:45:52');
/*!40000 ALTER TABLE `pets` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.reserva_salao_festas
DROP TABLE IF EXISTS `reserva_salao_festas`;
CREATE TABLE IF NOT EXISTS `reserva_salao_festas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_evento` varchar(255) NOT NULL,
  `from_unidade` int(11) NOT NULL,
  `datahora_evento` datetime NOT NULL,
  `datahora_cadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `from_cadastro` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_reserva_salao_festas_cadastro` (`from_cadastro`),
  KEY `FK_reserva_salao_festas_unidade` (`from_unidade`),
  CONSTRAINT `FK_reserva_salao_festas_cadastro` FOREIGN KEY (`from_cadastro`) REFERENCES `moradores` (`id`),
  CONSTRAINT `FK_reserva_salao_festas_unidade` FOREIGN KEY (`from_unidade`) REFERENCES `unidades` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.reserva_salao_festas: ~3 rows (aproximadamente)
DELETE FROM `reserva_salao_festas`;
/*!40000 ALTER TABLE `reserva_salao_festas` DISABLE KEYS */;
INSERT INTO `reserva_salao_festas` (`id`, `titulo_evento`, `from_unidade`, `datahora_evento`, `datahora_cadastro`, `from_cadastro`) VALUES
	(1, 'Baile das Flores de Outono', 9, '2021-02-19 18:30:00', '2022-03-30 15:44:00', 11),
	(2, 'Festa de Aniversario do Pedro', 4, '2022-05-10 17:00:00', '2022-03-30 15:44:04', 8),
	(3, 'Churrasco do Tio Felipe', 10, '2023-03-13 12:30:00', '2022-03-30 15:44:07', 12);
/*!40000 ALTER TABLE `reserva_salao_festas` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.unidades
DROP TABLE IF EXISTS `unidades`;
CREATE TABLE IF NOT EXISTS `unidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_condominio` int(11) NOT NULL,
  `from_bloco` int(11) NOT NULL,
  `numUni` int(11) NOT NULL,
  `metraUni` int(11) NOT NULL,
  `vagasUni` int(11) NOT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT concat(curdate(),' ',curtime()),
  `dataUpdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK4` (`from_condominio`),
  KEY `FK5` (`from_bloco`),
  CONSTRAINT `FK4` FOREIGN KEY (`from_condominio`) REFERENCES `condominios` (`id`),
  CONSTRAINT `FK5` FOREIGN KEY (`from_bloco`) REFERENCES `blocos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.unidades: ~23 rows (aproximadamente)
DELETE FROM `unidades`;
/*!40000 ALTER TABLE `unidades` DISABLE KEYS */;
INSERT INTO `unidades` (`id`, `from_condominio`, `from_bloco`, `numUni`, `metraUni`, `vagasUni`, `dataCadastro`, `dataUpdate`) VALUES
	(1, 1, 1, 101, 20, 3, '2022-04-01 11:52:33', '2022-04-01 11:54:49'),
	(2, 2, 2, 201, 20, 3, '2022-04-01 11:52:33', '2022-04-01 11:54:50'),
	(3, 3, 3, 301, 20, 3, '2022-04-01 11:52:33', '2022-04-01 11:54:51'),
	(4, 4, 4, 102, 30, 1, '2022-04-01 11:52:33', '2022-04-01 11:54:54'),
	(5, 5, 5, 201, 30, 1, '2022-04-01 11:52:33', '2022-04-01 11:54:56'),
	(6, 6, 6, 301, 30, 1, '2022-04-01 11:52:33', '2022-04-01 11:54:57'),
	(7, 7, 7, 401, 10, 2, '2022-04-01 11:52:33', '2022-04-04 11:06:39'),
	(8, 8, 8, 201, 10, 2, '2022-04-01 11:52:33', '2022-04-01 11:54:58'),
	(9, 9, 9, 301, 10, 2, '2022-04-01 11:52:33', '2022-04-01 11:54:58'),
	(10, 10, 10, 401, 50, 2, '2022-04-01 11:52:33', '2022-04-01 11:54:59'),
	(11, 11, 11, 324, 20, 1, '2022-04-01 11:52:33', '2022-04-04 11:07:03'),
	(12, 12, 12, 201, 30, 4, '2022-04-01 11:52:33', '2022-04-01 11:55:00'),
	(15, 1, 14, 201, 20, 3, '2022-04-04 10:21:53', '2022-04-04 10:23:46'),
	(16, 2, 15, 231, 20, 3, '2022-04-04 10:21:53', '2022-04-04 11:06:45'),
	(17, 3, 16, 312, 20, 3, '2022-04-04 10:21:53', '2022-04-04 11:06:46'),
	(18, 4, 17, 423, 20, 3, '2022-04-04 10:21:53', '2022-04-04 11:06:48'),
	(19, 5, 18, 512, 20, 3, '2022-04-04 10:21:53', '2022-04-04 11:06:50'),
	(20, 6, 19, 123, 20, 3, '2022-04-04 10:21:53', '2022-04-04 11:06:51'),
	(21, 7, 20, 412, 20, 3, '2022-04-04 10:21:53', '2022-04-04 11:06:52'),
	(22, 8, 21, 523, 20, 3, '2022-04-04 10:21:53', '2022-04-04 11:06:54'),
	(23, 9, 22, 535, 20, 3, '2022-04-04 10:21:53', '2022-04-04 11:06:56'),
	(24, 10, 24, 333, 20, 3, '2022-04-04 10:21:53', '2022-04-04 11:06:58'),
	(25, 1, 1, 203, 30, 2, '2022-04-06 14:56:52', '2022-04-06 14:56:52');
/*!40000 ALTER TABLE `unidades` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.usuarios
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `usuario` varchar(255) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT concat(curdate(),' ',curtime()),
  `dataUpdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.usuarios: ~2 rows (aproximadamente)
DELETE FROM `usuarios`;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id`, `nome`, `usuario`, `senha`, `dataCadastro`, `dataUpdate`) VALUES
	(1, 'Henrique da Silva Cardoso', 'henrique', '202cb962ac59075b964b07152d234b70', '2022-04-06 16:03:03', '2022-04-06 16:03:59'),
	(6, 'Fernando Senna Carlinhos', 'fernandindobalaio', '8f15e198e56d02271a2b4c48412d53e8', '2022-04-06 17:00:10', '2022-04-06 17:00:10');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
	