<?
Class Unidade extends Bloco{
    function __construct(){

    }

    function getUnidade($id = null) {
        $qry = 'SELECT 
        con.nomeCondo,
        blo.nomeBloco,
        uni.id,
        uni.numUni,
        uni.metraUni,
        uni.vagasUni,
        uni.from_condominio,
        uni.from_bloco,
        uni.dataCadastro,
        uni.dataUpdate
        FROM
        unidades uni 
        INNER JOIN condominios con ON con.id = uni.from_condominio
        INNER JOIN blocos blo ON blo.id = uni.from_bloco ';
        
        $contaTermos = count($this->busca);
        if($contaTermos) {
            $i = 0;
            foreach($this->busca as $field => $termo) {
                if($i == 0 && $termo != null) {
                    $qry .= ' WHERE ';
                    $i++;
                }
                switch($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.'uni.'.$field.' = '.$termo.' AND ';
                        }
                        break;
                        default:
                        $qry = $qry.'uni.'.$field.' LIKE "%'.$termo.'%"'.' AND ';
                        break;
                    }
            }
            $qry = rtrim($qry, ' AND');
        }
        if ($id) {
            $qry .= ' WHERE uni.id = '.$id;
            $unique = true;
        }
        return $this->listData($qry, $unique);
    }

    function getUniFromBlo($blo) {
        $qry = 'SELECT id, numUni FROM unidades WHERE from_bloco = '.$blo;
        return $this->listData($qry);
    }

    function setUnidade($dados) {
        $values = '';
        $qry = 'INSERT INTO unidades (';
        foreach($dados as $ch => $value) {
            $qry .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $qry = rtrim($qry,', ');
        $qry .=') VALUES ('.rtrim($values,', ').')';
        return $this->insertData($qry);        
    }

    function editUnidade($dados) {
        $qry = 'UPDATE unidades SET ';

        foreach($dados as $ch=>$value) {
            if($ch != 'editar') {
                $qry .="`".$ch."` = '".$value."', ";
            }
        }

        $qry = rtrim($qry,', ');
        $qry .= ' WHERE id ='.$dados['editar'];
        return $this->updateData($qry);
    }

    function deletaUnidade($id) {
        $qry = 'DELETE FROM unidades WHERE id ='.$id;
        return $this->deleteData($qry);
    }
}
?>