<?
Class Condominio extends Administradora {

    function __construct(){

    }

    function getCondo($id = null) {
        $qry = 'SELECT 
        con.id,
        con.nomeCondo,
        con.qtBloco,
        con.logra,
        con.numEnd,
        con.bairro,
        con.cidade,
        con.estado,
        con.cep,
        admin.nomeAdmin,
        con.dataCadastro,
        con.dataUpdate
        FROM 
        condominios con
        INNER JOIN administradoras admin ON admin.id = con.from_admin ';
        $contaTermos = count($this->busca);
        if($contaTermos) {
            $i = 0;
            foreach($this->busca as $field => $termo) {
                if($i == 0 && $termo != null) {
                    $qry .= ' WHERE ';
                    $i++;
                }
                
                switch($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.'con.'.$field.' = '.$termo.' AND ';
                        }
                        break;
                    default:
                        $qry = $qry.'con.'.$field.' LIKE "%'.$termo.'%"'.' AND ';
                        break;
                }
            }
            $qry = rtrim($qry, ' AND');
        }
        if ($id) {
            $qry .= ' WHERE con.id = '.$id;
            $unique = true;
        }
        return $this->listData($qry, $unique);
    }

    function setCondo($dados) {
        $values = '';
        $qry = 'INSERT INTO condominios (';
        foreach($dados as $ch => $value) {
            $qry .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $qry = rtrim($qry,', ');
        $qry .=') VALUES ('.rtrim($values,', ').')';
        return $this->insertData($qry);
    }

    function editCondo($dados) {
        $qry = 'UPDATE condominios SET ';

        foreach($dados as $ch=>$value) {
            if($ch != 'editar') {
                $qry .="`".$ch."` = '".$value."', ";
            }
        }

        $qry = rtrim($qry,', ');
        $qry .= ' WHERE id ='.$dados['editar'];
        return $this->updateData($qry);
    }

    function deletaCondo($id) {
        $qry = 'DELETE FROM condominios WHERE id ='.$id;
        return $this->deleteData($qry);
    }
}
?>