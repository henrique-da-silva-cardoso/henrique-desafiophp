<?
class Dashboard extends Dao {
    
    function __construct()
    {
        
    }

    function getMoradoresbyCondominio() {
        $qry = 'SELECT con.nomeCondo,
        COUNT(mor.nome) AS qt_moradores
        FROM
        condominios con
        RIGHT JOIN moradores mor ON con.id = mor.from_condominio
        GROUP BY con.nomeCondo';
        return $this->listData($qry);
    }

    function lastAdmins() {
        $qry = 'SELECT 
        nomeAdmin
        FROM
        administradoras 
        ORDER BY dataCadastro DESC
        LIMIT 5';
        return $this->listData($qry);
    }

    function getEveryCount() {
        $qry = 'SELECT 
        COUNT(mor.id) AS qtMoradores,
        (SELECT COUNT(condo.id) FROM condominios condo) AS qtCondominio,
        (SELECT COUNT(blo.id) FROM blocos blo) AS qtBloco,
        (SELECT COUNT(uni.id) FROM unidades uni) AS qtUnidade,
        (SELECT COUNT(admin.id) FROM administradoras admin) AS qtAdmin
        FROM
        moradores mor';
        return $this->listData($qry, true);
    }
}
?>