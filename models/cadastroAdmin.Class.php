<?
class Administradora extends Dao {
    function __construct(){

    }

    function getAdmin($id = null) {
        $qry = 'SELECT * FROM administradoras adm';
        $contaTermos = count($this->busca);
        if($contaTermos) {
            $i = 0;
            foreach($this->busca as $field => $termo) {
                if($i == 0 && $termo != null) {
                    $qry .= ' WHERE ';
                    $i++;
                }
                switch($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.'adm.'.$field.' = '.$termo.' AND ';
                        }
                        break;
                    default:
                        $qry = $qry.'adm.'.$field.' LIKE "%'.$termo.'%"'.' AND ';
                        break;
                }
            }
            $qry = rtrim($qry, ' AND');
        }
        if ($id) {
            $qry .= ' WHERE adm.id = '.$id;
            $unique = true;
        }
        return $this->listData($qry, $unique);
    }

    function setAdmin($dados) {
        $values = '';
        $qry = 'INSERT INTO administradoras (';
        foreach($dados as $ch => $value) {
            $qry .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $qry = rtrim($qry,', ');
        $qry .=') VALUES ('.rtrim($values,', ').')';
        return $this->insertData($qry);
    }

    function editAdmin($dados) {
        $qry = 'UPDATE administradoras SET ';

        foreach($dados as $ch=>$value) {
            if($ch != 'editar') {
                $qry .="`".$ch."` = '".$value."', ";
            }
        }

        $qry = rtrim($qry,', ');
        $qry .= ' WHERE id ='.$dados['editar'];
        return $this->updateData($qry);
    }

    function deleteAdmin($id) {
        $qry = 'DELETE FROM administradoras WHERE id ='.$id;
        return $this->deleteData($qry);
    }
}
?>