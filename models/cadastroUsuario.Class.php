<?
Class Usuarios extends Dao{
    function __construct(){

    }

    function getUsuario($id = null) {
        $qry = 'SELECT 
        *
        FROM
        usuarios user';
        $contaTermos = count($this->busca);
        if($contaTermos) {
            $i = 0;
            foreach($this->busca as $field => $termo) {
                if($i == 0 && $termo != null) {
                    $qry .= ' WHERE ';
                    $i++;
                }
                switch($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.'user.'.$field.' = '.$termo.' AND ';
                        }
                        break;
                        default:
                        $qry = $qry.'user.'.$field.' LIKE "%'.$termo.'%"'.' AND ';
                        break;
                    }
            }
            $qry = rtrim($qry, ' AND');
        }
        if ($id) {
            $qry .= ' WHERE id = '.$id;
            $unique = true;
        }
        return $this->listData($qry, $unique);
    }

    function userExists($user){
        $qry = "SELECT usuario FROM usuarios WHERE usuario = '".$user."'";
        return $this->listData($qry,true);
    }

    function setUsuario($dados) {
        $values = '';
        $qry = 'INSERT INTO usuarios (';
        foreach($dados as $ch => $value) {
            $qry .= '`'.$ch.'`, ';
            if($ch != 'senha'){
                $values .= "'".$value."', ";
            } else {
                $values .= "'".md5($value)."', ";
            }
        }
        $qry = rtrim($qry,', ');
        $qry .=') VALUES ('.rtrim($values,', ').')';
        return $this->insertData($qry);        
    }

    function editUsuario($dados) {
        $qry = 'UPDATE usuarios SET ';

        foreach($dados as $ch=>$value) {
            if($ch == 'senha') {
                $qry .="`".$ch."` = '".md5($value)."', ";
            } else if($ch != 'editar') {
                $qry .="`".$ch."` = '".$value."', ";
            }
        }

        $qry = rtrim($qry,', ');
        $qry .= ' WHERE id ='.$dados['editar'];
        return $this->updateData($qry);
    }

    function deletaUsuario($id) {
        $qry = 'DELETE FROM usuarios WHERE id ='.$id;
        return $this->deleteData($qry);
    }
}
?>