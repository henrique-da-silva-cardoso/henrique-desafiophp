<?
Class Bloco extends Condominio{
    protected $id;

    function __construct(){

    }

    function getBloco($id = null) {
        $qry = 'SELECT
        con.nomeCondo, 
        blo.nomeBloco, 
        blo.qtAndares, 
        blo.qtUnidades, 
        blo.dataCadastro, 
        blo.dataUpdate,
        blo.from_condominio,
        blo.id
        FROM 
        blocos blo 
        INNER JOIN condominios con ON con.id = blo.from_condominio ';
        $contaTermos = count($this->busca);
        if($contaTermos) {
            $i = 0;
            foreach($this->busca as $field => $termo) {
                if($i == 0 && $termo != null) {
                    $qry .= ' WHERE ';
                    $i++;
                }
                
                switch($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.'blo.'.$field.' = '.$termo.' AND ';
                        }
                        break;
                    default:
                        $qry = $qry.'blo.'.$field.' LIKE "%'.$termo.'%"'.' AND ';
                        break;
                }
            }
            $qry = rtrim($qry, ' AND');
        }
        if ($id) {
            $qry .= ' WHERE blo.id = '.$id;
            $unique = true;
        }
        return $this->listData($qry, $unique);
    }

    function getBlocoFromCond($cond) {
        $qry = 'SELECT id, nomeBloco FROM blocos WHERE from_condominio = '.$cond;
        return $this->listData($qry);
    }

    function setBloco($dados) {
        $values = '';
        $qry = 'INSERT INTO blocos (';
        foreach($dados as $ch => $value) {
            $qry .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $qry = rtrim($qry,', ');
        $qry .=') VALUES ('.rtrim($values,', ').')';
        return $this->insertData($qry);
    }

    function editBloco($dados) {
        $qry = 'UPDATE blocos SET ';

        foreach($dados as $ch=>$value) {
            if($ch != 'editar') {
                $qry .="`".$ch."` = '".$value."', ";
            }
        }

        $qry = rtrim($qry,', ');
        $qry .= ' WHERE id ='.$dados['editar'];
        return $this->updateData($qry);
    }

    function deletaBloco($id) {
        $qry = 'DELETE FROM blocos WHERE id ='.$id;
        return $this->deleteData($qry);
    }
}
?>