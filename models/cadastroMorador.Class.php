<?
Class Moradores extends Unidade{
    function __construct() {

    }

    function getMoradores($id = null) {
        $qry = 'SELECT con.nomeCondo, blo.nomeBloco, uni.numUni, mor.id, mor.nome, mor.cpf, mor.email, mor.telefone, mor.senha, mor.from_condominio, mor.from_bloco, mor.from_unidade, mor.dataCadastro, mor.dataUpdate FROM moradores mor INNER JOIN condominios con ON con.id = mor.from_condominio INNER JOIN blocos blo ON blo.id = mor.from_bloco INNER JOIN unidades uni ON uni.id = mor.from_unidade'; 
        $contaTermos = count($this->busca);
        if($contaTermos) {
            $i = 0;
            foreach($this->busca as $field => $termo) {
                if($i == 0 && $termo != null) {
                    $qry .= ' WHERE ';
                    $i++;
                }
                switch($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.'mor.'.$field.' = '.$termo.' AND ';
                        }
                        break;
                    default:
                        $qry = $qry.'mor.'.$field.' LIKE "%'.$termo.'%"'.' AND ';
                        break;
                }
            }
            $qry = rtrim($qry, ' AND');
        }
        if ($id) {
            $qry .= ' WHERE mor.id = '.$id;
            $unique = true;
        }
        return $this->listData($qry, $unique);
    }

    function setMoradores($dados) {
        $values = '';
        $qry = 'INSERT INTO moradores (';
        foreach($dados as $ch => $value) {
            $qry .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $qry = rtrim($qry,', ');
        $qry .=') VALUES ('.rtrim($values,', ').')';
        return $this->insertData($qry);
    }

    function editaCliente($dados) {
        $qry = 'UPDATE condominios SET ';

        foreach($dados as $ch=>$value) {
            if($ch != 'editar') {
                $qry .="`".$ch."` = '".$value."', ";
            }
        }

        $qry = rtrim($qry,', ');
        $qry .= ' WHERE id ='.$dados['editar'];
        return $this->updateData($qry);
    }

    function deletaMorador($id){
        $qry = 'DELETE FROM moradores WHERE id ='.$id;
        return $this->deleteData($qry);
    }
}
