<?
Class conFis extends Condominio{
    
    function __construct(){

    }

    function getConFis($id = null) {
        $qry = "SELECT con.nomeCondo,
        confis.id,
        confis.funcao,
        confis.nome,
        confis.dataCadastro,
        confis.dataUpdate
        FROM
        confis
        INNER JOIN condominios con ON con.id = confis.from_condominio";
        $contaTermos = count($this->busca);
        if($contaTermos) {
            $i = 0;
            foreach($this->busca as $field => $termo) {
                if($i == 0 && $termo != null) {
                    $qry .= ' WHERE ';
                    $i++;
                }
                
                switch($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.'confis.'.$field.' = '.$termo.' AND ';
                        }
                        break;
                    default:
                        $qry = $qry.'confis.'.$field.' LIKE "%'.$termo.'%"'.' AND ';
                        break;
                }
            }
            $qry = rtrim($qry, ' AND');
        }
        if($id) {
            $qry .= ' WHERE confis.id = '.$id;
            $unique = true;
        }
        $qry .= " ORDER BY con.id";
        return $this->listData($qry, $unique);
    }

    function setConFis($dados) {
        $values = '';
        $qry = 'INSERT INTO confis (';
        foreach($dados as $ch => $value) {
            $qry .= '`'.$ch.'`, ';
            if($ch != 'senha'){
                $values .= "'".$value."', ";
            } else {
                $values .= "'".md5($value)."', ";
            }
        }
        $qry = rtrim($qry,', ');
        $qry .=') VALUES ('.rtrim($values,', ').')';
        return $this->insertData($qry);        
    }

    function editConFis($dados) {
        $qry = 'UPDATE confis SET ';

        foreach($dados as $ch=>$value) {
            if($ch != 'editar') {
                $qry .="`".$ch."` = '".$value."', ";
            }
        }

        $qry = rtrim($qry,', ');
        $qry .= ' WHERE id ='.$dados['editar'];
        return $this->updateData($qry);
    }

    function deletaConFis($id) {
        $qry = 'DELETE FROM confis WHERE id ='.$id;
        return $this->deleteData($qry);
    }
}
?>