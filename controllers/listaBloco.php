<?
$bloco = new Bloco();
$bloco->pagination = 4;

if (isset($_GET['b'])) {
    $montaBusca = array();
    foreach ($_GET['b'] as $field => $termo) {
        switch ($field) {
            case 'termo1':
                $montaBusca['nomeBloco'] = $termo;
                break;
            default:
                break;
        }
    }
}

$bloco->busca = $montaBusca;
$result = $bloco->getBloco();

$paginacao = ($result['totalResult'] > $bloco->pagination) ? $bloco->renderPagination($result['qtPaginas']) : '';
$totalRegistros = ($result['totalResult'] < 10 ? '0' . $result['totalResult'] : $result['totalResult']);
?>