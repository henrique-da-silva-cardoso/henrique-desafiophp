<?
$condo = new Condominio();
$condo->pagination = 6;

if (isset($_GET['b'])) {
    $montaBusca = array();
    foreach ($_GET['b'] as $field => $termo) {
        switch ($field) {
            case 'termo1':
                $montaBusca['nomeCondo'] = $termo;
                break;
            default:
                break;
        }
    }
}

$condo->busca = $montaBusca;
$result = $condo->getCondo();


$paginacao = ($result['totalResult'] > $condo->pagination) ? $condo->renderPagination($result['qtPaginas']) : '';
$totalRegistros = ($result['totalResult'] < 10 ? '0' . $result['totalResult'] : $result['totalResult']);
?>