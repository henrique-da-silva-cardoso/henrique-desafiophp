<?
$usuario = new Usuarios();
$usuario->pagination = 4;

if (isset($_GET['b'])) {
    $montaBusca = array();
    foreach ($_GET['b'] as $field => $termo) {
        switch ($field) {
            case 'termo1    ':
                $montaBusca['nome'] = $termo;
                break;
            default:
                break;
        }
    }
}

$usuario->busca = $montaBusca;
$result = $usuario->getUsuario();

$paginacao = ($result['totalResult'] > $usuario->pagination) ? $usuario-> renderPagination($result['qtPaginas']) : '';
$totalRegistros =($result['totalResult'] < 10 ? '0' . $result['totalResult'] : $result['totalResult']);
?>
