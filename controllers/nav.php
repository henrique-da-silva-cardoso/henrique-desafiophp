<?
$nav = array(
    'Cadastros' => array(
        'cadastroAdmin' => 'Cadastro Administradora',
        'cadastroCondo' => 'Cadastro Condomínio',
        'cadastroBloco' => 'Cadastro Bloco',
        'cadastroUnidade' => 'Cadastro Unidades',
        'cadastroMorador' => 'Cadastro Moradores',
        'cadastroConFis' => 'Cadastro Conselho Fiscal',
        'cadastroUsuario' => 'Cadastro Usuarios'
    ),
    'Listagens' => array(
        'listaAdmin' => 'Administradoras',
        'listaCondo' => 'Condomínios',
        'listaBloco' => 'Blocos',
        'listaUnidade' => 'Unidades',
        'listaMorador' => 'Moradores',
        'listaConFis' => 'Conselhos Fiscais',
        'listaUsuario' => 'Usuarios'
    )
);
?>