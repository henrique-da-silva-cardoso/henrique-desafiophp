<?
$morador = new Moradores();
$morador->pagination = 3;

if(isset($_GET['b'])){
    $montaBusca = array();
    foreach($_GET['b'] as $field => $termo) {
        switch ($field) {
            case 'termo1':
                $montaBusca['nome'] = $termo;
                break;
            case 'termo2':
                $montaBusca['from_condominio'] = $termo;
                break;
            default:
                break;
        }
    }
}

$morador->busca = $montaBusca;
$result = $morador->getMoradores();

$condominio = new Condominio();
$listCondominio = $condominio->getCondo();

$paginacao = ($result['totalResult'] > $morador->pagination) ? $morador->renderPagination($result['qtPaginas']) : '';
$totalRegistros =($result['totalResult'] < 10 ? '0' . $result['totalResult'] : $result['totalResult']);
?>