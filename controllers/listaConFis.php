<?
$confis = new conFis();
$confis->pagination = 5;

if (isset($_GET['b'])) {
    $montaBusca = array();
    foreach ($_GET['b'] as $field => $termo) {
        switch ($field) {
            case 'termo1':
                $montaBusca['nome'] = $termo;
                break;
            default:
                break;
        }
    }
}

$confis->busca = $montaBusca;
$result = $confis->getConFis();

$paginacao = ($result['totalResult'] > $confis->pagination) ? $confis->renderPagination($result['qtPaginas']) : '';
$totalRegistros = ($result['totalResult'] < 10 ? '0' . $result['totalResult'] : $result['totalResult']);
?>