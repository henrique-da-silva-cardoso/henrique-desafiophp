<?
$unidade = new Unidade();
$unidade->pagination = 4;

if (isset($_GET['b'])) {
    $montaBusca = array();
    foreach ($_GET['b'] as $field => $termo) {
        switch ($field) {
            case 'termo2':
                $montaBusca['numUni'] = $termo;
                break;
            default:
                break;
        }
    }
}


$unidade->busca = $montaBusca;
$result = $unidade->getUnidade();

$paginacao = ($result['totalResult'] > $unidade->pagination) ? $unidade->renderPagination($result['qtPaginas']) : '';
$totalRegistros =($result['totalResult'] < 10 ? '0' . $result['totalResult'] : $result['totalResult']);
?>