<?
$admin = new Administradora();
$admin->pagination = 3;

if (isset($_GET['b'])) {
    $montaBusca = array();
    foreach ($_GET['b'] as $field => $termo) {
        switch ($field) {
            case 'termo1':
                $montaBusca['nomeAdmin'] = $termo;
                break;
            case 'termo2':
                $montaBusca['cnpj'] = $termo;
                break;
            default:
                break;
        }
    }
}

$admin->busca = $montaBusca;
$result = $admin->getAdmin();

$administradora = new Administradora();
$result1 = $administradora->getAdmin()['resultSet'];

$paginacao = ($result['totalResult'] > $admin->pagination) ? $admin->renderPagination($result['qtPaginas']) : '';
$totalRegistros = ($result['totalResult'] < 10 ? '0' . $result['totalResult'] : $result['totalResult']);
?>