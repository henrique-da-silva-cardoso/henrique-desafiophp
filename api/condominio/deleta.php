<?
require "../../funcoes.php";

$condo = new Condominio();

if($condo->deletaCondo($_POST['id'])) {

    $totalRegistros = $condo->getCondo()['totalResult'];

    $result = array(
        'status' => 'success',
        'totalRegistros' => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        'msg' => 'Condomínio deletado com sucesso'
    );

    echo json_encode($result);
} else {
    $result = array(
        'status' => 'danger',
        'msg' => 'O condomínio não pode ser deletado'
    );

    echo json_encode($result);
}
?>