<?
require "../../funcoes.php";

$condo = new Condominio();

if($condo->editCondo($_POST)) {
    $result = array(
        'status' => 'success',
        'msg' => 'Condomínio editado com sucesso.'
    );
    
    echo json_encode($result);
} else {
    $result = array(
        'status' => 'danger',
        'msg' => 'O condomínio não pode ser editado'
    );

    echo json_encode($result);
}
?>