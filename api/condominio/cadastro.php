<?
require "../../funcoes.php";

$condominio = new Condominio();

if($condominio->setCondo($_POST)) {
    $result = array(
        'status' => 'success',
        'msg' => 'Condomínio cadastrado com sucesso.'
    );
    
    echo json_encode($result);
} else {
    $result = array(
        'status' => 'danger',
        'msg' => 'O condomínio não pode ser cadastrado'
    );

    echo json_encode($result);
}
?>