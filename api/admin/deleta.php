<?
require "../../funcoes.php";

$admin = new Administradora();

if($admin->deleteAdmin($_POST['id'])) {

    $totalRegistros = $admin->getAdmin()['totalResult'];

    $result = array(
        'status' => 'success',
        'totalRegistros' => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        'msg' => 'Administradora deletada com sucesso'        
    );

    echo json_encode($result);
} else {
    $result = array(
        'status' => 'danger',
        'msg' => 'A administradora não pode ser deletada'
    );

    echo json_encode($result);
}
?>