<?
require "../../funcoes.php";

$admin = new Administradora();

if($admin->editAdmin($_POST)) {
    $result = array(
        'status' => 'success',
        'msg' => 'Administradora editada com sucesso.'
    );
    
    echo json_encode($result);
} else {
    $result = array(
        'status' => 'danger',
        'msg' => 'A administradora não pode ser editada'
    );

    echo json_encode($result);
}
?>