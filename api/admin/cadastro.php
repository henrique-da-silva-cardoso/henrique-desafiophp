<?
require "../../funcoes.php";

$admin = new Administradora();

if($admin->setAdmin($_POST)) {
    $result = array(
        'status' => 'success',
        'msg' => 'Administradora cadastrado com sucesso.'
    );
    
    echo json_encode($result);
} else {
    $result = array(
        'status' => 'danger',
        'msg' => 'A administradora não pode ser cadastrada'
    );

    echo json_encode($result);
}
?>