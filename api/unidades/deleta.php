<?
require "../../funcoes.php";

$uni = new Unidade();

if($uni->deletaUnidade($_POST['id'])) {
    
    $totalRegistros = $uni->getUnidade()['totalResult'];

    $result = array(
        'status' => 'success',
        'totalRegistros' => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        'msg' => 'Unidade deletada com sucesso',
        
    );

    echo json_encode($result);
} else {
    $result = array(
        'status' => 'danger',
        'msg' => 'A unidade não pode ser deletada'
    );

    echo json_encode($result);
}
?>