<?
require "../../funcoes.php";

$uni = new Unidade();

if($uni->editUnidade($_POST)) {
    $result = array(
        'status' => 'success',
        'msg' => 'Unidade editada com sucesso.'
    );
    
    echo json_encode($result);
} else {
    $result = array(
        'status' => 'danger',
        'msg' => 'A unidade não pode ser editada'
    );

    echo json_encode($result);
}
?>