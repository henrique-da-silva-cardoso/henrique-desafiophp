<?
require "../../funcoes.php";

$uni = new Unidade();

if($uni->setUnidade($_POST)) {
    $result = array(
        'status' => 'success',
        'msg' => 'Unidade cadastrada com sucesso.'
    );
    
    echo json_encode($result);
} else {
    $result = array(
        'status' => 'danger',
        'msg' => 'A unidade não pode ser cadastrada'
    );

    echo json_encode($result);
}
?>