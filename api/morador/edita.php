<?
require "../../funcoes.php";

$cliente = new Moradores();

if($cliente->editaCliente($_POST)) {
    $result = array(
        'status' => 'success',
        'msg' => 'Cliente editado com sucesso.'
    );
    
    echo json_encode($result);
} else {
    $result = array(
        'status' => 'danger',
        'msg' => 'O cliente não pode ser editado'
    );

    echo json_encode($result);
}
?>