<?
require "../../funcoes.php";

$morador = new Moradores();

if($morador->deletaMorador($_POST['id'])) {

    $totalRegistros = $morador->getMoradores()['totalResult'];

    $result = array(
        'status' => 'success',
        'totalRegistros' => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        'msg' => 'Morador deletado com sucesso',
        
    );

    echo json_encode($result);
} else {
    $result = array(
        'status' => 'danger',
        'msg' => 'O morador não pode ser deletado'
    );

    echo json_encode($result);
}
?>