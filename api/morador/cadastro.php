<?
require "../../funcoes.php";

$moradores = new Moradores();

if($moradores->setMoradores($_POST)) {
    $result = array(
        'status' => 'success',
        'msg' => 'Cliente cadastrado com sucesso.'
    );
} else {
    $result = array(
        'status' => 'danger',
        'msg' => 'O cliente não pode ser cadastrado'
    );
}
echo json_encode($result);
?>