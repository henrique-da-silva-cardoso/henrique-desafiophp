<?
require "../../funcoes.php";

$blocos = new Bloco();
$dados = $blocos->getBlocoFromCond($_REQUEST['id']);

if(!empty($dados)) {
    $result = array(
        "status" => 'success',
        "resultSet" => $dados['resultSet']
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => 'danger',
        "msg" => 'O cadastro não pode ser inserido'
    );

    echo json_encode($result);
}
?>