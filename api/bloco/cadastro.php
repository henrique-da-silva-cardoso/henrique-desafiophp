<?
require "../../funcoes.php";

$bloco = new Bloco();

if($bloco->setBloco($_POST)) {
    $result = array(
        'status' => 'success',
        'msg' => 'Bloco cadastrado com sucesso.'
    );
    
    echo json_encode($result);
} else {
    $result = array(
        'status' => 'danger',
        'msg' => 'O bloco não pode ser cadastrado'
    );

    echo json_encode($result);
}
?>