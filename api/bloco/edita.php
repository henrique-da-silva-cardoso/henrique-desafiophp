<?
require "../../funcoes.php";

$bloco = new Bloco();

if($bloco->editBloco($_POST)) {
    $result = array(
        'status' => 'success',
        'msg' => 'Bloco editado com sucesso.'
    );
    
    echo json_encode($result);
} else {
    $result = array(
        'status' => 'danger',
        'msg' => 'O bloco não pode ser editado'
    );

    echo json_encode($result);
}
?>