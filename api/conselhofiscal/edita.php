<?
require "../../funcoes.php";

$con = new conFis();

if($con->editConFis($_POST)) {
    $result = array(
        'status' => 'success',
        'msg' => 'Conselho editado com sucesso.'
    );
    
    echo json_encode($result);
} else {
    $result = array(
        'status' => 'danger',
        'msg' => 'O Conselho não pode ser editado'
    );

    echo json_encode($result);
}
?>