<?
require "../../funcoes.php";

$con = new conFis();

if($con->setConFis($_POST)) {
    $result = array(
        'status' => 'success',
        'msg' => 'Conselho cadastrado com sucesso.'
    );
    
    echo json_encode($result);
} else {
    $result = array(
        'status' => 'danger',
        'msg' => 'O Conselho não pode ser cadastrado'
    );

    echo json_encode($result);
}
?>