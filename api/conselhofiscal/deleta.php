<?
require "../../funcoes.php";

$con = new conFis();

if($con->deletaConFis($_POST['id'])) {
    $totalRegistros = count($_SESSION['unidade']);

    $result = array(
        'status' => 'success',
        'totalRegistros' => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        'msg' => 'Conselho deletado com sucesso',
        
    );

    echo json_encode($result);
} else {
    $result = array(
        'status' => 'danger',
        'msg' => 'O Conselho não pode ser deletado'
    );

    echo json_encode($result);
}
?>