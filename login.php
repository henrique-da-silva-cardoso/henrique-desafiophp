<?
include 'funcoes.php';
// session_destroy();
//array que gerará a nav

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="icofont/icofont.min.css">
    <link rel="icon" href="apcontrole.png" class="icone">
    <title>AP Controle</title>
</head>

<body>
    <main class='container text-center display:flex; justify-content:center;'>
            <div class="col-4 m-auto" style="padding-top: 15%;">
                <form class="form-signin p-5" action="<?=$url_site?>controllers/restrito.php" method="POST" style="border-radius:20px; box-shadow: 0px 5px 5px 2px rgb(0 0 0 / 49%);">
                    <img class="mb-4" src="imgs/apcontrole.png" alt="" width="100" height="100">
                    <h1 class="h3 mb-3 font-weight-normal text-light">ap.controle</h1>
                    <input type="text" id="inputEmail" name="usuario" class="form-control" placeholder="Usuario" required autofocus>
                    <input type="password" id="inputPassword" name="senha" class="form-control" placeholder="Password" required>
                    <button class="btn btn-lg amarelo texto-branco btn-block" type="submit">Sign in</button>
                </form>
            </div>
    </main>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/app.js?v=" <? rand(0, 9999) ?>></script>
    <script type="text/javascript">
        <?if(isset($_GET['msg'])) {?>
            $(function (){
                myAlert('danger','<?=$_GET['msg']?>', 'main');
            })
        <?}?>
    </script>
</body>

</html>