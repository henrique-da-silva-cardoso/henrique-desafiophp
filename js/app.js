$(function () {
    //cadastra ou edita clientes
    $('#formCliente').submit(function () {
        var editar = $(this).find('input[name=editar]').val();
        $('.buttonEnviar').attr('disabled', true);

        if (editar) {
            var url = url_site+'api/morador/edita.php';
            var urlRedir = url_site+'listaMorador';
        } else {
            var url = url_site+'api/morador/cadastro.php';
            var urlRedir = url_site+'cadastroMorador';
        }

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })
        return false;
    });

    //deleta clientes
    $(document).on('click', '.removerMorador', function () {
        var idRegistro = $(this).attr('data-id');
        var urlRedir = url_site+'listaMorador';
        $.ajax({
            url: url_site+'api/morador/deleta.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main')
                }
            }
        })
        return false;
    });

    $('#formCondo').submit(function () {
        console.log('teste')
        var editar = $(this).find('input[name=editar]').val();
        $('.buttonEnviar').attr('disabled', true);

        if (editar) {
            var url = url_site+'api/condominio/edita.php';
            var urlRedir = url_site+'listaCondo'
        } else {
            var url = url_site+'api/condominio/cadastro.php';
            var urlRedir = url_site+'cadastroCondo'
        }

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })
        return false;
    });

    $(document).on('click', '.removerCondo', function () {
        var idRegistro = $(this).attr('data-id');
        var urlRedir = url_site+'listaCondo';
        $.ajax({
            url: url_site+'api/condominio/deleta.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main')
                }
            }
        })
        return false;
    });

    $('#formBloco').submit(function () {
        var editar = $(this).find('input[name=editar]').val();
        $('.buttonEnviar').attr('disabled', true);

        if (editar) {
            var url = url_site+'api/bloco/edita.php';
            var urlRedir = url_site+'listaBloco'
        } else {
            var url = url_site+'api/bloco/cadastro.php';
            var urlRedir = url_site+'cadastroBloco'
        }

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })
        return false;
    });

    $(document).on('click', '.removerBloco', function () {
        var idRegistro = $(this).attr('data-id');
        var urlRedir = url_site+'listaBloco';
        $.ajax({
            url: url_site+'api/bloco/deleta.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main')
                }
            }
        })
        return false;
    });

    $('#formUnidade').submit(function () {
        var editar = $(this).find('input[name=editar]').val();
        $('.buttonEnviar').attr('disabled', true);

        if (editar) {
            var url = url_site+'api/unidades/edita.php';
            var urlRedir = url_site+'listaUnidade'
        } else {
            var url = url_site+'api/unidades/cadastro.php';
            var urlRedir = url_site+'cadastroUnidade'
        }

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })
        return false;
    });

    $(document).on('click', '.removerUnidade', function () {
        var idRegistro = $(this).attr('data-id');
        var urlRedir = url_site+'listaUnidade';
        $.ajax({
            url: url_site+'api/unidades/deleta.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main')
                }
            }
        })
        return false;
    });

    $('#formUsuario').submit(function() {
        var editar = $(this).find('input[name=editar]').val();
        // $('.buttonEnviar').attr('disabled', true);
        
        if (editar) {
            var url = url_site+'api/usuario/edita.php';
            var urlRedir = url_site+'listaUsuario'
        } else {
            var url = url_site+'api/usuario/cadastro.php';
            var urlRedir = url_site+'cadastroUsuario'
        }
        
        var senha = $(this).find('#senha').val();
        var confirmaSenha = $(this).find('#csenha').val();

        if (senha == confirmaSenha) {
            $.ajax({    
                url: url,
                dataType: 'json',
                type: 'POST',
                data: $(this).serialize(),
                success: function (data) {
                    if (data.status == 'success') {
                        myAlert(data.status, data.msg, 'main', urlRedir);
                    } else {
                        myAlert(data.status, data.msg, 'main', urlRedir);
                    }
                }
            })
        } else {
            myAlert('danger','A senha digitada no campo de confirmação não confere!', 'main', urlRedir);
        }

        return false;
    });

    $(document).on('click', '.removerUsuario', function () {
        var idRegistro = $(this).attr('data-id');
        var urlRedir = url_site+'listaUsuario';
        $.ajax({
            url: url_site+'api/usuario/deleta.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main')
                }
            }
        })
        return false;
    });

    $('#formConFis').submit(function () {
        var editar = $(this).find('input[name=editar]').val();
        $('.buttonEnviar').attr('disabled', true);

        if (editar) {
            var url = url_site+'api/conselhofiscal/edita.php';
            var urlRedir = url_site+'listaConFis'
        } else {
            var url = url_site+'api/conselhofiscal/cadastro.php';
            var urlRedir = url_site+'cadastroConFis'
        }

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })
        return false;
    });

    $(document).on('click', '.removerConFis', function () {
        var idRegistro = $(this).attr('data-id');
        var urlRedir = url_site+'listaConFis';
        $.ajax({
            url: url_site+'api/conselhofiscal/deleta.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main')
                }
            }
        })
        return false;
    });

    $('#formAdmin').submit(function () {
        console.log('teste')
        var editar = $(this).find('input[name=editar]').val();
        $('.buttonEnviar').attr('disabled', true);

        if (editar) {
            var url = url_site+'api/admin/edita.php';
            var urlRedir = url_site+'listaAdmin'
        } else {
            var url = url_site+'api/admin/cadastro.php';
            var urlRedir = url_site+'cadastroAdmin'
        }

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })
        return false;
    });

    $(document).on('click', '.removerAdmin', function () {
        var idRegistro = $(this).attr('data-id');
        var urlRedir = url_site+'listaAdmin';
        $.ajax({
            url: url_site+'api/admin/deleta.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main')
                }
            }
        })
        return false;
    });

    $(document).on('change', '.fromCondominio', function () {
        selecionado = $(this).val();

        $.ajax({
            url: url_site+'api/uteis/listBlocos.php',
            dataType: 'json',
            type: 'POST',
            data: { id: selecionado },
            success: function (data) {
                selectPopulation('.fromBloco', data.resultSet, 'nomeBloco');
            }
        })
    })

    $(document).on('change', '.fromBloco', function () {
        selecionado = $(this).val();

        $.ajax({
            url: url_site+'api/uteis/listUnidades.php',
            dataType: 'json',
            type: 'POST',
            data: { id: selecionado },
            success: function (data) {
                selectPopulation('.fromUnidade', data.resultSet, 'numUni');
            }
        })
    })

    function selectPopulation(seletor, dados, field) {
        console.log('unidade');
        estrutura = '<option value="">Selecione...</option>';

        for (let i = 0; i < dados.length; i++) {
            estrutura += '<option value = "' + dados[i].id + '">' + dados[i][field] + '</option>';
        }

        $(seletor).html(estrutura);
    }

    // $(document).on('change', 'input[name="sindicoProfissional"]',function() {

    //     var profissional = $(this).val();
    //     if (profissional == 1) {
    //         $('.sindico').html('<input type="text" class="form-control" name="nomeSindico" aria-describedby="required" value="<?= $nomeSindico ?>" placeholder="Nome do Síndico" required>')
    //     } else {
    //         $('.sindico').html('<input type="text" class="form-control" name="nomeSindico" aria-describedby="required" value="<?= $nomeSindico ?>" placeholder="Nome do Síndico" required>')
    //     }
    // })

    $('input[name="sindicoProfissional"]').on('change', function () {
        var molho = $(this).val();
        if (molho == 1) {
            $('.qtMolho').removeClass('d-none')
        } else {
            $('.qtMolho').addClass('d-none')
        }
    })

    $('#filtro').on('submit',function(){
        var pagina = $('input[name=page]').val();
        var termo1 = $('.termo1').val();
        // var termo1 = $('.termo1').unmask().val();
        var termo2 = $('.termo2').val();


        termo1 = (termo1) ? termo1+'/' : '';
        termo2 = (termo2) ? termo2+'/' : '';
    
        window.location.href = url_site+pagina+'/busca/'+termo1+termo2
        
        return false
    }) 
    
    $(document).on('keyup focusout change', '.termo1, .termo2', function(){
        var termo1 = $('.termo1').val();
        var termo2 = $('.termo2').val();
    
        if(termo1 || termo2) {
            $('button[type="submit"]').prop('disabled', false);
        } else {
            $('button[type="submit"]').prop('disabled', true);
        }
    })

    var options = {
        onKeyPress: function(cpf, e, field, options) {
            console.log(cpf.length)
            var masks = ['+00 (00) 0000-0000', '+00 (00) 0 0000-0000']
            var mask = (cpf.lenght >=17) ? masks[1] : masks[0];
            $('input[name="telefone"]').mask(mask, options);
        }
    }

    

});

function myAlert(tipo, mensagem, pai, url) {
    url = (url == undefined ? url == '' : url);
    componente = '<div class="alert alert-' + tipo + '" role="alert">' + mensagem + '</div>';

    $(pai).prepend(componente);

    setTimeout(function () {
        $(pai).find('div.alert').remove();
        //vai redirecionar
        if (tipo == 'success' && url) {
            setTimeout(function () {
                window.location.href = url;
            }, 500);
        } else if (tipo == 'danger' && url) {
            setTimeout(function () {
                window.location.href = url;
            }, 500);
        }
    }, 500);
}
